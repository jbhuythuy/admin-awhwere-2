import './scss/application.scss';
import Routes from './global/router';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Toaster } from 'react-hot-toast';
import Header from './Layout/Header/Header';
import Footer from './Layout/Footer/Footer';
import Sidebar from './Layout/Sidebar/Sidebar';
import SignIn from './containers/Auth/SignIn/SignIn';
import img from './assets/img/anime3.png'

function Page() {
  return(
    <div class="wrapper">
      <Sidebar/>
      <Toaster
        position="top-right"
        reverseOrder={false}
      />
      <div class="main-panel bg-blue" data="blue">
        <Header/>
        <div class="content">
          <div class="row">
            <div class="col-md-12">
              <Routes />
              {/* <div class="card">
                <div class="card-body">
                  <Routes />
                </div>
              </div> */}
            </div>
          </div>
        </div>
        <Footer/>
      </div>
    </div>
  )
}

function App() {
  const token = localStorage.getItem('token');

  return (
    <BrowserRouter>
      {token ? <Page /> : <SignIn/>}
    </BrowserRouter>
  );
}

export default App;
