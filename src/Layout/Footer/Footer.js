function Footer(){
  return(
    <footer class="footer">
      <div class="container-fluid">
        <ul class="nav">
          <li class="nav-item">
            <a href="disanquangninh.gov.vn" class="nav-link">
            <i class="fas fa-arrow-circle-left pr-2"></i>
              www.disanquangninh.gov.vn
            </a>
          </li>
        </ul>
        <div class="copyright text-center">
          <p>© 2019 Bản quyền thuộc về Sở Văn hóa và Thể thao tỉnh Quảng Ninh.</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
