import './header.scss';
import vn from '../../assets/icons/vn.svg';
import React, { useState, useEffect, useRef } from 'react';
import {LogoutService} from '../../services/logout.service';
import { UserService } from '../../services/user.service';
import User from '../../assets/img/anime3.png'
import Logo from '../../assets/icons/Logo-DSVH.svg'
import useOutsideClick from './useOutsideClick';

function Header() {
  const [isUser, setUser] = useState(false);
  const [infoUser, setInfoUser] = useState({});
  const ref = useRef();

  useEffect(() => {
    let info = JSON.parse(localStorage.getItem('currentUser'))
		UserService.getById(info.id).then(item => {
			if(item.data.length > 0){
				setInfoUser(item.data[0])
			}
		})
  }, []);

  function logOut(){
    const token = localStorage.getItem('token')
    LogoutService.logout({token: token})
      .then(() => {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
        window.location.reload();
      })
  }

  function showUser(){
    setUser(!isUser)
  }

  useOutsideClick(ref, () => {
    setUser(false)
  });

  return(
    <nav class="navbar navbar-expand-lg navbar-absolute navbar-transparent">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <div class="navbar-toggle d-inline">
            <button type="button" class="navbar-toggler">
              <span class="navbar-toggler-bar bar1"></span>
              <span class="navbar-toggler-bar bar2"></span>
              <span class="navbar-toggler-bar bar3"></span>
            </button>
          </div>
          <div className='logohead'>
            <img src={Logo}></img>
          </div>
          {/* <a class="navbar-brand" href="javascript:void(0)">User Profile</a> */}
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        <div class="collapse navbar-collapse" id="navigation">
          <ul class="navbar-nav ml-auto">
            <li class="search-bar input-group">
              <button class="btn btn-link" id="search-button" data-toggle="modal" data-target="#searchModal">
              </button>
            </li>
            <p className='col-md-5 pt-3 pl-4'>{ infoUser !== null && infoUser.name }</p>
            <li class="dropdown nav-item" ref={ref}>
              <a class="dropdown-toggle nav-link" data-toggle="dropdown" onClick={() => showUser() }>
                <div class="photo">
                  <img src={User} alt="Profile Photo"/>
                </div>
                <b class="caret d-none d-lg-block d-xl-block"></b>
                <p class="d-lg-none">
                  Log out
                </p>
              </a>
              <ul class={`dropdown-menu dropdown-navbar ${isUser ? 'show-menu-user' : '' }`} ref={ref} >
                <li class="nav-link"><a href="/user-infomation" class="nav-item dropdown-item">Tài khoản</a></li>
                <li class="dropdown-divider"></li>
                <li class="nav-link"><a onClick={()=> logOut() } class="nav-item dropdown-item">Đăng xuất</a></li>
              </ul>
            </li>
            <li class="separator d-lg-none"></li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Header;
