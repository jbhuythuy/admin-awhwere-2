import './sidebar.scss';

function Sidebar(){
  return(
    <div className="sidebar" data="blue">
      <div className="sidebar-wrapper">
        <ul className="nav">
          <li>
            <a href={`/home`}>
              <i className="fa fa-tachometer" aria-hidden="true"></i>
              <p>Tổng Quan</p>
            </a>
          </li>
          <li>
            <a href={`/heritages`}>
              <i className="fa fa-pie-chart" aria-hidden="true"></i>
              <p>Quản Lý Di Sản</p>
            </a>
          </li>
          <li>
            <a href={`/list-profile`}>
              <i className="fa fa-folder-open-o" aria-hidden="true"></i>
              <p>Hồ Sơ Di Sản</p>
            </a>
          </li>
          <li>
            <a href={`/list-users`}>
              <i className="fa fa-users" aria-hidden="true"></i>
              <p>Tài Khoản</p>
            </a>
          </li>
          <li>
            <a href={`/news-manager`}>
              <i className="fa fa-newspaper-o" aria-hidden="true"></i>
              <p>Quản Lý Tin Tức</p>
            </a>
          </li>
          <li>
            <a href={`/data-rating`}>
              <i className="fas fa-band-aid"></i>
              <p>Xếp Hạng Di Sản</p>
            </a>
          </li>
          <li>
            <a href={`/data-areas`}>
              <i className="fas fa-location-arrow"></i>
              <p>Địa Điểm Di Sản</p>
            </a>
          </li>
          <li className="active-pro">
            <a href={`/data-type`}>
              <i className="fab fa-accusoft"></i>
              <p>Loại Hình Di Sản</p>
            </a>
          </li>
          <li className="active-pro">
            <a href={`/data-nation`}>
              <i className="fas fa-address-book"></i>
              <p>Dân Tộc</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
