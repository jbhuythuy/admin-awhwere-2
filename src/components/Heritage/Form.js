import React, { useState, useEffect, Component } from 'react';
import { useHistory } from 'react-router-dom';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useParams, useLocation} from 'react-router-dom';
import "react-datepicker/dist/react-datepicker.css";
import toast from 'react-hot-toast';
import ValidateForm from './Validate';
import 'filepond/dist/filepond.min.css';
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileEncode from 'filepond-plugin-file-encode';
import './form.scss'
import { HeritageService } from '../../services/heritage.service';
import { ApiService } from '../../services/api.service';
import Select from 'react-select';
import { FixedSizeList as List } from "react-window";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

registerPlugin(FilePondPluginFileValidateType, FilePondPluginFileEncode);

const Form = () => {
  const history = useHistory();
  const { id } = useParams();
  let query = useQuery();
  let type = query.get("type");
  const isAddMode = !id;
  const validateForm = yup.object().shape(ValidateForm(type, yup));
  const [files, setFiles] = useState([])
  const [files3d, setFiles3d] = useState([])
  const [fileTopos, setFileTopos] = useState([])
  const [fileImages, setFileImages] = useState([])
  const [fileImagesTwo, setFileImagesTwo] = useState([])
  const [fileVideos, setFileVideos] = useState([])
  const [fileSounds, setFileSounds] = useState([])
  const [fileDocuments, setFileDocuments] = useState([])
  const [listDataType, setlistDataType] = useState([]);
  const [listDataRating, setlistDataRating] = useState([]);
  const [listDataAreas, setlistDataAreas] = useState([]);
  const [listHeritages, setlistHeritages] = useState([]);
  const [done, setDone] = useState(false);

  const model_kinds = 'kinds';
  const model_ratings = 'ratings';
  const model_places = 'places';

  const { register, handleSubmit, setValue, control,  reset, formState:{ errors } } = useForm({
    resolver: yupResolver(validateForm)
  });

  function onSubmit(data, files, files3d, fileImages, fileVideos, fileSounds, fileDocuments, fileImagesTwo, fileTopos) {
    setDone(true)
    let params_files =  files.map(file => ({
      type: file.fileType,
      size: file.fileSize,
      name: file.filename,
      base64: file.getFileEncodeBase64String()
    }))

    let params_file3d =  files3d.map(file => ({
      type: file.fileType,
      size: file.fileSize,
      name: file.filename,
      base64: file.getFileEncodeBase64String()
    }))

    let params_file_images =  fileImages.map(file => ({
      type: file.fileType,
      size: file.fileSize,
      name: file.filename,
      base64: file.getFileEncodeBase64String()
    }))

    let params_file_videos =  fileVideos.map(file => ({
      type: file.fileType,
      size: file.fileSize,
      name: file.filename,
      base64: file.getFileEncodeBase64String()
    }))

    let params_file_sounds =  fileSounds.map(file => ({
      type: file.fileType,
      size: file.fileSize,
      name: file.filename,
      base64: file.getFileEncodeBase64String()
    }))

    let params_file_documents =  fileDocuments.map(file => ({
      type: file.fileType,
      size: file.fileSize,
      name: file.filename,
      base64: file.getFileEncodeBase64String()
    }))

    let params_file_topos = fileTopos.map(file => ({
      type: file.fileType,
      size: file.fileSize,
      name: file.filename,
      base64: file.getFileEncodeBase64String()
    }))

    let data_vr360 = { vr360: { files: params_files }}
    let data_vr3d = { vr3d: { files: params_file3d }}
    let data_images = { images: { files: params_file_images }}
    let data_videos = { videos: { files: params_file_videos }}
    let data_sounds = { sounds: { files: params_file_sounds }}
    let data_documents = { documents: { files: params_file_documents }}
    let vr3d_topos = { vr3d_topos: { files: params_file_topos }}

    data = {...data, ...data_vr360}
    data = {...data, ...data_vr3d}
    data = {...data, ...data_images}
    data = {...data, ...data_videos}
    data = {...data, ...data_sounds}
    data = {...data, ...data_documents}
    data = {...data, ...vr3d_topos}

    if(fileImagesTwo.length > 0){
      let image = {
        "base64": fileImagesTwo[0].getFileEncodeBase64String(),
        "name": fileImagesTwo[0].filename,
        "size": fileImagesTwo[0].fileSize
      }

      let result = { image: image }
      data = {...data, ...result}
    }

    if(data.parent_heritage_id !== undefined) {
      Object.assign(data, { parent_id: data.parent_heritage_id.value });
    }

    if(data.se_place_id !== undefined) {
      Object.assign(data, { place_id: data.se_place_id.value });
    }

    if(data.se_kind_id !== undefined) {
      Object.assign(data, { kind_id: data.se_kind_id.value });
    }

    if(data.se_rating_id !== undefined) {
      Object.assign(data, { rating_id: data.se_rating_id.value });
    }

    if(data.parent_heritage_id !== undefined) {
      delete data.parent_heritage_id
    }

    if(data.se_place_id !== undefined) {
      delete data.se_place_id
    }

    if(data.se_kind_id !== undefined) {
      delete data.se_kind_id
    }

    if(data.se_rating_id !== undefined) {
      delete data.se_rating_id
    }

    return isAddMode
      ? createHeritage(data)
      : updateHeritage(id, data);
  }

  function createHeritage(data) {
    HeritageService.create(data)
      .then(() => {
        toast.success('Tạo di sản thành công')
        history.push('/heritages');
      })
  }

  function updateHeritage(id, data) {
    HeritageService.update(id, data)
      .then(() => {
        toast.success('Cập nhật di sản thành công')
        history.push('/heritages');
      })
  }

  useEffect(() => {
    if (!isAddMode) {
      HeritageService.getById([id])
        .then(heritage => {
          if(heritage.data !== undefined && heritage.data[0] !== undefined) {
            const field_name = heritage.data[0]
            const fields = [
              'name', 'decision', 'coordinate', 'worshipper',
              'status', 'take_place_time', 'cultural_subject', 'note', 'date_rating', 'introduction'
            ];
            const fields2 = ['kind_id', 'rating_id', 'place_id']
            fields.forEach(field => setValue(field, field_name[field]));
            fields2.forEach(field => setValue(field, `${field_name[field]}`));
          }
        });
    }

    ApiService.getForAll(model_kinds).then(item => {
      let data = item.data.map(file => ({
        value: file.id,
        label: file.name,
      }))

      setlistDataType(data)
    })

    ApiService.getForAll(model_ratings).then(item => {
      let data = item.data.map(file => ({
        value: file.id,
        label: file.name,
      }))

      setlistDataRating(data)
    })

    ApiService.getForAll(model_places).then(item => {
      let data = item.data.map(file => ({
        value: file.id,
        label: file.name,
      }))

      setlistDataAreas(data)
    })
    HeritageService.getAllHeritages().then(item => {
      setlistHeritages(item.data)
    })
  }, []);

  return(
    <form onSubmit={handleSubmit(data => onSubmit(data, files, files3d, fileImages, fileVideos, fileSounds, fileDocuments, fileImagesTwo, fileTopos))}  onReset={ reset }>
      <div className='row pl-0 pr-0 bg -blue-light area'>
        <div className='col-lg-12 pl-2'>
          <div className='row mb-2 pt-2'>
            <div className='col-lg-6'>
              {(()=>{
                if(isAddMode){
                  return(
                    <h7 className='uppercase'>tạo di sản</h7>
                  )
                }
                else {
                  return(
                    <h7 className='uppercase'>chỉnh sửa di sản</h7>
                  )
                }
              })()}
              <a href={'/heritages'} className='i-btn -primary ml-20'>
                Quay Lại
              </a>
              <button type='submit' className={`i-btn -success ml-20 ${ done ? '-disabled': '' }`}
                >{isAddMode ? 'Lưu' : 'Cập Nhật'}
                { done &&
                  <span className="spinner-border spinner-border-sm ml-1"></span>
                }
              </button>
            </div>
            <div className='col-lg-6 '>
              <div className='i-breadcrumb float-right'>
                <a className='i-breadcrumb -section'>Admin</a>
                <i className='i-breadcrumb -icon-divider'></i>
                <a className='i-breadcrumb -section'>di sản</a>
                <i className='i-breadcrumb -icon-divider'></i>
                <div className='i-breadcrumb'>tạo di sản</div>
              </div>
            </div>
          </div>
          <div className='row pb-10 pt-20 bg -white form-heritage'>
            <div className='col-lg-5'>
              <div class='i-group'>
                <div class='i-label i-text'>Tên:</div>
                <input {...register('name')} className={ `i-input ${ errors.name ? '-danger' : '' }`}
                  placeholder='Nhập Tên' />
                { errors.name && (
                  <p className='error-danger'>{ errors.name?.message }</p>
                )}
              </div>
              <div className='i-group'>
                <div className='i-label i-text'>Loại Hình Di Sản:</div>
                <Controller
                  name="se_kind_id"
                  control={control}
                  render={({ field, value }) => <Select
                    {...field}
                    options={listDataType}
                    placeholder={'Chọn giá trị'}
                    isClearable={true}
                    value={value}
                  />}
                />

                {/* <select {...register('kind_id')} className={ `i-select ${ errors.kind_id ? '-danger' : '' }`}>
                  <option value=''>Chọn giá trị</option>
                  {listDataType.length > 0 && listDataType.map(item => (
                    <option key={item.id} value={ item.id }>{ item.name }</option>
                  ))}
                </select>
                { errors.kind_id && (
                  <p className='error-danger'>{ errors.kind_id?.message }</p>
                )} */}
              </div>
              <div className='i-group'>
                <div className='i-label i-text'>Xếp Hạng Di Sản:</div>
                <Controller
                  name="se_rating_id"
                  control={control}
                  render={({ field }) => <Select
                    {...field}
                    options={listDataRating}
                    placeholder={'Chọn giá trị'}
                    isClearable={true}
                  />}
                />

                {/* <select {...register('rating_id')} className={ `i-select ${ errors.rating_id ? '-danger' : '' }`}>
                  <option value=''>Chọn giá trị</option>
                  {listDataRating.length > 0 && listDataRating.map(item => (
                    <option key={item.id} value={ item.id }>{ item.name }</option>
                  ))}
                </select>
                { errors.rating_id && (
                  <p className='error-danger'>{ errors.rating_id?.message }</p>
                )} */}
              </div>

              <div className='i-group'>
                <div className='i-label i-text'>Quyết Định:</div>
                <input {...register('decision')} className='i-input' placeholder='Nhập Quyết Định' />
              </div>
              <div className='i-group'>
                <div className='i-label i-text'>Toạ Độ:</div>
                <input {...register('coordinate')} className='i-input' placeholder='Nhập Toạ Độ' />
              </div>

              <div className='i-group'>
                <div className='i-label i-text'>Nhân Vật Tôn Thờ:</div>
                <input {...register('worshipper')} className='i-input'placeholder='Nhập Nhân Vật Tôn Thờ' />
              </div>

              <div className='i-group'>
                <div className='i-label i-text'>Trạng Thái:</div>
                <input {...register('status')} className='i-input'placeholder='Nhập Trạng Thái' />
              </div>

              <div className='i-group'>
                <div className='i-label i-text'>Thời Gian Diễn Ra:</div>
                <input {...register('take_place_time')} className='i-input'placeholder='Thời Gian Diễn Ra' />
              </div>
              <div className='i-group'>
                <div className='i-label i-text'>Chủ Đề Văn Hoá:</div>
                <input {...register('cultural_subject')} className='i-input'placeholder='Nhập Chủ Đề Văn Hoá' />
              </div>
              <div className='i-group'>
                <label className='i-label i-text'>Ghi Chú:</label>
                <textarea {...register('note')} className='i-textarea' />
              </div>

            </div>
            <div className='col-lg-7'>
              <div className='i-group'>
                <div className='i-label i-text'>Thuộc di sản cha:</div>
                <Controller
                  name="parent_heritage_id"
                  control={control}
                  render={({ field }) => <Select
                    {...field}
                    components={{ MenuList }}
                    options={listHeritages}
                    placeholder={'Chọn giá trị'}
                    isClearable={true}
                  />}
                />
              </div>

              <div className='i-group'>
                <div className='i-label i-text'>Thời Gian Xếp Hạng:</div>
                <input {...register('date_rating')} className='i-input'placeholder='Thời Gian Xếp Hạng' />
              </div>
              <div className='i-group'>
                <div className='i-label i-text'>Địa Điểm:</div>
                <Controller
                  name="se_place_id"
                  control={control}
                  render={({ field }) => <Select
                    {...field}
                    options={listDataAreas}
                    placeholder={'Chọn giá trị'}
                    isClearable={true}
                  />}
                />
                {/* <select {...register('place_id')} className={ `i-select ${ errors.place_id ? '-danger' : '' }`}>
                  <option value=''>Chọn giá trị</option>
                  {listDataAreas.length > 0 && listDataAreas.map(item => (
                    <option key={item.id} value={ item.id }>{ item.name }</option>
                  ))}
                </select>
                { errors.place_id && (
                  <p className='error-danger'>{ errors.place_id?.message }</p>
                )} */}
              </div>

              <div className='i-group'>
                <label className='i-label i-text'>Giới Thiệu Chung:</label>
                <textarea {...register('introduction')} className='i-textarea min-text-area' rows='22' />
              </div>
            </div>
          </div>

          <div className='row bg -white form-heritage'>
            <div className='col-lg-11 bg -white pb-3'>
              <p className='i-label i-text'>File Ảnh Đại Diện:</p>
              <FilePond
                files={fileImagesTwo}
                allowMultiple={false}
                onupdatefiles={setFileImagesTwo}
                labelIdle='Tải File'
                allowFileTypeValidation={true}
                acceptedFileTypes= {['image/*']}
                labelFileTypeNotAllowed='File không hợp lệ'
                fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file ảnh'
                allowFileEncode={true}
              />
            </div>

            <div className='col-lg-11 bg -white'>
              <p className='i-label i-text'>File VR</p>
              <FilePond
                files={files}
                allowMultiple={true}
                onupdatefiles={setFiles}
                labelIdle='Tải File'
                allowFileTypeValidation={true}
                acceptedFileTypes= {['application/zip', 'application/x-zip-compressed']}
                labelFileTypeNotAllowed='File không hợp lệ'
                fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file zip'
                allowFileEncode={true}
              />
            </div>

            <div className='col-lg-11 bg -white'>
              <p className='i-label i-text'>File 3D Địa Hình:</p>
              <FilePond
                files={fileTopos}
                allowMultiple={true}
                onupdatefiles={setFileTopos}
                labelIdle='Tải File'
                allowFileTypeValidation={true}
                acceptedFileTypes= {['application/zip', 'application/x-zip-compressed']}
                labelFileTypeNotAllowed='File không hợp lệ'
                fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file zip'
                allowFileEncode={true}
              />
            </div>

            <div className='col-lg-11 bg -white'>
              <p className='i-label i-text'>File 3D:</p>
              <FilePond
                files={files3d}
                allowMultiple={true}
                onupdatefiles={setFiles3d}
                labelIdle='Tải File'
                allowFileTypeValidation={true}
                acceptedFileTypes= {['application/zip', 'application/x-zip-compressed']}
                labelFileTypeNotAllowed='File không hợp lệ'
                fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file zip'
                allowFileEncode={true}
              />
            </div>

            <div className='col-lg-11 bg -white'>
              <p className='i-label i-text'>File Ảnh:</p>
              <FilePond
                files={fileImages}
                allowMultiple={true}
                onupdatefiles={setFileImages}
                labelIdle='Tải File'
                allowFileTypeValidation={true}
                acceptedFileTypes= {['image/*']}
                labelFileTypeNotAllowed='File không hợp lệ'
                fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file ảnh'
                allowFileEncode={true}
              />
            </div>

            <div className='col-lg-11 bg -white'>
              <p className='i-label i-text'>File Video:</p>
              <FilePond
                files={fileVideos}
                allowMultiple={true}
                onupdatefiles={setFileVideos}
                labelIdle='Tải File'
                allowFileTypeValidation={true}
                acceptedFileTypes= {['application/zip', 'application/x-zip-compressed']}
                labelFileTypeNotAllowed='File không hợp lệ'
                fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file zip'
                allowFileEncode={true}
              />
            </div>

            <div className='col-lg-11 bg -white'>
              <p className='i-label i-text'>File Âm Thanh:</p>
              <FilePond
                files={fileSounds}
                allowMultiple={true}
                onupdatefiles={setFileSounds}
                labelIdle='Tải File'
                allowFileTypeValidation={true}
                acceptedFileTypes= {['application/zip', 'application/x-zip-compressed']}
                labelFileTypeNotAllowed='File không hợp lệ'
                fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file zip'
                allowFileEncode={true}
              />
            </div>

            <div className='col-lg-11 bg -white'>
              <p className='i-label i-text'>File Tài Liệu:</p>
              <FilePond
                files={fileDocuments}
                allowMultiple={true}
                onupdatefiles={setFileDocuments}
                labelIdle='Tải File'
                allowFileTypeValidation={true}
                acceptedFileTypes= {
                  ['application/pdf', 'text/plain', 'application/msword',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/rtf', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
                }
                labelFileTypeNotAllowed='File không hợp lệ'
                fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file tài liệu'
                allowFileEncode={true}
              />
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Form;

class MenuList extends Component {
  render() {
    const { options, children, maxHeight, getValue } = this.props;
    const [value] = getValue();
    const initialOffset = options.indexOf(value) * 35;

    return (
      <List
        height={maxHeight}
        itemCount={children.length}
        itemSize={35}
        initialScrollOffset={initialOffset}
      >
        {({ index, style }) => <div style={style}>{children[index]}</div>}
      </List>
    );
  }
}
