import { Link } from "react-router-dom";
import React, { useState, useEffect, useRef } from 'react';
import toast from 'react-hot-toast';
import { HeritageService } from '../../services/heritage.service';
import moment from 'moment';
import '../../containers/Heritage/heritage.scss'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

function BodyTable(props) {
  function deleteHeritage(id) {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui-confirm'>
            <h1 className='title'>Bạn chắc chứ?</h1>
            <p className='content'>Bạn thực sự muốn xoá dữ liệu?</p>
            <button onClick={onClose}>Không</button>
            <button
              onClick={() => handleClickDelete(onClose, id)}
            >
              Có
            </button>
          </div>
        );
      }
    })
  }

  function handleClickDelete(onClose, id){
    HeritageService.delete(id)
      .then(() => {
        props.setAreas(items => items.filter(x => x.id !== id));
        toast.success('Xoá di sản thành công');
      });

    onClose();
  }

  if(props.heritages !== undefined && props.heritages.length > 0) {
    const content = props.heritages.map((heritage, index) =>
      <tr key={ heritage.id }>
        <td>{ index + 1 }</td>
        <td>{ heritage.name }</td>
        <td>{heritage.rating_name}</td>
        <td>{heritage.kind_name}</td>
        <td>{heritage.place_name}</td>
        <td>{ props.moment(heritage.create_date).format('L') }</td>
        <td className="text-nowrap">
          <a className="mr-2" href={`/heritages-show/${ heritage.id }`} >
            <i className="icon icon-eye"></i>
          </a>
          {props.roleAdmin &&
            <>
              <a className="mr-2" href={`/heritages-edit/${ heritage.id }`}>
                <i className="icon icon-edit -sm"></i>
              </a>
              <a className='cursor-pointer' onClick={() => deleteHeritage(heritage.id)}>
                <i className="icon icon-trash"></i>
              </a>
            </>
          }

        </td>
      </tr>
    );

    return (
      <tbody>
        { content }
      </tbody>
    );
  }
  else {
    return (
      <tbody>
      </tbody>
    );
  }
}

function List() {
  const [areas, setAreas] = useState([]);
  const [limitPage, setLimitPage] = useState(10)
  const [totalPage, settotalPage] = useState(0)
  const [currentPage, setcurrentPage] = useState(1)
  const [offsetPage, setoffsetPage] = useState(0)
  const [isSearch, setSearch] = useState(false)
  const [valueSearch, setvalueSearch] = useState('')
  const [roleAdmin, setRole] = useState(true)
  const [places, setPlaces] = useState([]);
  const [filter, setfilter] = useState('null');
  const textInput = useRef(null);

  useEffect(() => {
    HeritageService.getAllList('null', limitPage, 0).then(item => {
      setAreas(item.data)
      settotalPage(Math.ceil(parseInt(item.total) / limitPage))
    });

    let info = JSON.parse(localStorage.getItem('currentUser'))
    let isAdmin = info.role === 'manager'
    setRole(isAdmin)

    HeritageService.getAllPlaces().then(item => {
      setPlaces(item.data);
    })
  }, []);

  function search(event) {
    setfilter('null')
    if (event.charCode === 13) {
      if(event.target.value == ''){
        setSearch(!isSearch)


        HeritageService.getAllList('null', limitPage, 0).then(item => {
          setAreas(item.data)
          settotalPage(Math.ceil(parseInt(item.total) / limitPage))
        })
      }
      else {
        setSearch(!isSearch)
        setvalueSearch(event.target.value)
        setcurrentPage(1)

        HeritageService.searchName(event.target.value, limitPage, 0)
          .then(item => {
            setAreas(item.data)
            settotalPage(Math.ceil(parseInt(item.total) / limitPage))
          })
      }
    }
  }

  function showSetLimitPage(value){
    setLimitPage(value)

    HeritageService.getAllList(filter, value, 0).then(item => {
      setAreas(item.data)
      settotalPage(Math.ceil(parseInt(item.total) / value))
    });
  }

  function showPageChange(value) {
    const offset = limitPage * (value -1)
    setcurrentPage(value)
    setoffsetPage(offset)

    if(isSearch) {
      HeritageService.searchName(valueSearch, limitPage, offset)
        .then(item => {
          setAreas(item.data)
        })
    }
    else {
      HeritageService.getAllList(filter, limitPage, offset).then(item => {
        setAreas(item.data)
      })
    }
  }

  function showFilterPage(value) {
    setfilter(value)
    setcurrentPage(1)
    textInput.current.value =''

    HeritageService.getAllList(value, limitPage, 0).then(item => {
      setAreas(item.data)
      settotalPage(Math.ceil(parseInt(item.total) / limitPage))
    });
  }

  return(
    <div className='row pl-0 pr-0 bg -blue-light area'>
      <div className='col-lg-12'>
        <div className='row pt-15 mb-10'>
            <div className='col-lg-6'>
              <p className='uppercase font-14 i-bold'>danh sách di sản</p>
            </div>
            <div className='col-lg-6'>
              <div className="i-breadcrumb float-right">
                <a className="i-breadcrumb -section">Admin</a>
                <i className="i-breadcrumb -icon-divider"></i>
                <a className="i-breadcrumb -section">Di sản</a>
                <i className="i-breadcrumb -icon-divider"></i>
                <div className="i-breadcrumb">Danh Sách</div>
              </div>
            </div>
          </div>
        <div className='row pb-2 pt-20 bg -white'>
          <div className='col-lg-12 pb-2'>
            <Link to={`/heritages-new`} className={roleAdmin ? '' : 'bt-disabled'}>
              <button className="i-btn -brown" disabled={roleAdmin ? '' : 'disabled'}>
                <i className="fas fa-plus-circle mr-1 ml-n2"></i>
                tạo di sản
              </button>
            </Link>
          </div>
        </div>

        <div className='row pb-10 bg -white'>
          <div className='col-lg-3 search'>
            <p className='mr-3'>Hiển Thị</p>
            <div className="i-group">
              <select className="i-search" onChange={e => showSetLimitPage(parseInt(e.currentTarget.value))}>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="50">50</option>
              </select>
            </div>
            <p className='ml-3'>Di Sản</p>
          </div>
          <div className="col-lg-6 filter justify-content-center">
            <p className='mr-1'>Khu vực:&emsp;
            </p>
            <div className="i-group">
              <select className="i-search" onChange={e => showFilterPage(e.currentTarget.value)} value={filter}>
                <option value={'null'} key={'filter'}>Bỏ lọc khu vực</option>
                <option value={0} key={0}>Không thuộc khu vực</option>
                {
                  places.length > 0 && places.map((item, index) => (
                      <option value={item.id} key={item.id}>{item.name}</option>
                  ))
                }
              </select>
            </div>
          </div>
          <div className='col-lg-3'>
            <input type="text" className="i-input" placeholder='Tìm Kiếm' onKeyPress={e => search(e)} ref={textInput}/>
          </div>
        </div>

        <div className='row bg -white min-table'>
          <div className='col-lg-12 box-list'>
            <div className="i-table-wrap" >
              <table className="i-table">
                <thead>
                  <tr>
                    <th>#.</th>
                    <th>Tên</th>
                    <th>Xếp Hạng</th>
                    <th>Loại Hình</th>
                    <th>Địa Điểm</th>
                    <th>Ngày Tạo</th>
                    <th></th>
                  </tr>
                </thead>
                <BodyTable heritages={areas} setAreas={setAreas} moment={moment} roleAdmin={roleAdmin} />
              </table>
            </div>
          </div>
        </div>
        <div className='i-border-down'></div>
        <div className='row  bg -white pt-10'>
          <div className='col-lg-7'>
          </div>
          <div className='col-lg-5 text-right'>
            <ul className="i-pagination">
              { currentPage > 1 &&
                <li className="i-pagination__item -text">
                  <a onClick={()=> showPageChange(currentPage - 1) }>
                    « Prev
                  </a>
                </li>
              }
              { currentPage > 1 &&
                <li className="i-pagination__item">
                  <a onClick={()=> showPageChange(currentPage - 1)}>
                    { currentPage - 1 }
                  </a>
                </li>
              }
              { totalPage > 0 &&
                <li className="i-pagination__item active">{currentPage}</li>
              }
              { totalPage > currentPage &&
                <li className="i-pagination__item">
                  <a onClick={()=> showPageChange(currentPage + 1)}>
                    {currentPage + 1}
                  </a>
                </li>}
              { totalPage > currentPage && <li className="i-pagination__item"><a>…</a></li> }
              { totalPage > currentPage &&
                <li className="i-pagination__item -text">
                  <a onClick={()=>showPageChange(currentPage + 1)}>
                    Next »
                  </a>
                </li>
              }
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default List;
