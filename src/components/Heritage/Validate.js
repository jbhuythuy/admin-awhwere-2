
function ValidateForm(type, yup){
  let objectValidate = {
    name: yup.string().required('Tên không được trống')
  }

  // if(type === 'place') {
  //   let place_id = { place_id: yup.string().required('Địa điểm không được trống') }
  //   objectValidate = { ...objectValidate, ...place_id };
  // }

  // if(type === 'rating') {
  //   let rating_id = { rating_id: yup.string().required('Xếp hạng không được trống') }
  //   objectValidate = { ...objectValidate, ...rating_id };
  // }

  // if(type === 'kind') {
  //   let kind_id = { kind_id: yup.string().required('Loại hình không được trống') }
  //   objectValidate = { ...objectValidate, ...kind_id };
  // }

  return objectValidate;
};

export default ValidateForm;
