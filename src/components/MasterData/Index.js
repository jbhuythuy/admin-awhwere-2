// import '../master_data.scss';
import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { ApiService } from '../../services/api.service';
import toast from 'react-hot-toast';
import ValidateForm from './Validate';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

function IndexMaster(props){
  const [isActive, setActive] = useState(false);
  const [isCreate, setCreate] = useState(false);
  const [isSearch, setSearch] = useState(false);
  const [valueSearch, setvalueSearch] = useState('');
  const [isId, setisId] = useState(null);
  const validateForm = yup.object().shape(ValidateForm(yup));
  const [listDataNation, setlistDataNation] = useState([]);
  const [currentPage, setcurrentPage] = useState(1);
  const [limitPage, setLimitPage] = useState(10);
  const [offsetPage, setoffsetPage] = useState(0)
  const [totalPage, settotalPage] = useState(0)
  const model_name = props.model_name;
  const [title, setTitle] = useState('')
  const [roleAdmin, setRole] = useState(true)

  const {
    register, handleSubmit, setValue, control,
    reset, formState:{ errors }, clearErrors
  } = useForm({
      resolver: yupResolver(validateForm)
    });

  function onSubmit(data) {
    return isCreate
    ? createMasterData(data)
    : updateMasterData(data);
  }

  function updateMasterData(data){
    setActive(!isActive);

    ApiService.update(model_name, isId, data)
      .then(() => {
        toast.success('Cập nhật dữ liệu thành công')

        ApiService.getAll(model_name, limitPage, offsetPage).then(item => {
          setlistDataNation(item.data)
        })
      })
  }

  function createMasterData(data) {
    setActive(!isActive);

    ApiService.create(model_name, data)
      .then(() => {
        toast.success('Tạo dữ liệu thành công')

        ApiService.getAll(model_name, limitPage, 0).then(item => {
          setlistDataNation(item.data)
          settotalPage(Math.ceil(parseInt(item.total) / limitPage))
        })
      })
  }

  function deleteMasterData(id) {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui-confirm'>
            <h1 className='title'>Bạn chắc chứ?</h1>
            <p className='content'>Bạn thực sự muốn xoá dữ liệu?</p>
            <button onClick={onClose}>Không</button>
            <button
              onClick={() => handleClickDelete(onClose, id)}
            >
              Có
            </button>
          </div>
        );
      }
    })
  }

  function handleClickDelete(onClose, id){
    ApiService.delete(model_name, id)
      .then(() => {
        setlistDataNation(items => items.filter(item => item.id !== id));
        toast.success('Xoá dữ liệu thành công');
      });

    onClose();
  }

  useEffect(() => {
    ApiService.getAll(model_name, limitPage, offsetPage).then(item => {
      setlistDataNation(item.data)
      settotalPage(Math.ceil(parseInt(item.total) / limitPage))
    })

    if(model_name === 'nations'){
      setTitle('Dân Tộc')
    }
    else {
      setTitle('Địa Điểm')
    }

    let info = JSON.parse(localStorage.getItem('currentUser'))
    let isAdmin = info.role === 'manager'
    setRole(isAdmin)
  }, []);

  function showModaData(check, id){
    clearErrors('name');
    if(check) {
      setisId(null);
      setValue('name', '')
    }
    else {
      setisId(id);
      ApiService.getById(model_name, id)
        .then(obj => {
          if(obj.data !== undefined){
            const field_name = obj.data
            const fields = ['name']
            fields.forEach(field => setValue(field, field_name[field]));
          }
        });
    }
    setCreate(check);
    setActive(!isActive);
  }

  function search(event) {
    if (event.charCode === 13) {
      if(event.target.value == ''){
        setSearch(!isSearch)

        ApiService.getAll(model_name, limitPage, 0).then(item => {
          setlistDataNation(item.data)
          settotalPage(Math.ceil(parseInt(item.total) / limitPage))
        })
      }
      else {
        setSearch(!isSearch)
        setvalueSearch(event.target.value)

        ApiService.search(model_name, `table.c.name.like(('%${ event.target.value }%'))`, limitPage, 0)
          .then(item => {
            setlistDataNation(item.data)
            settotalPage(Math.ceil(parseInt(item.total) / limitPage))
          })
      }
    }
  }

  function showPageChange(value) {
    const offset = limitPage * (value -1)
    setcurrentPage(value)
    setoffsetPage(offset)

    if(isSearch) {
      ApiService.search(model_name, `table.c.name.like(('%${ valueSearch }%'))`, limitPage, offset)
        .then(item => {
          setlistDataNation(item.data)
        })
    }
    else {
      ApiService.getAll(model_name, limitPage, offset).then(item => {
        setlistDataNation(item.data)
      })
    }
  }

  function showSetLimitPage(value){
    setLimitPage(value)

    ApiService.getAll(model_name, value, 0).then(item => {
      setlistDataNation(item.data)
      settotalPage(Math.ceil(parseInt(item.total) / value))
    })
  }

  return(
    <div className='row pl-0 pr-0 bg -blue-light area'>
      <div className='col-lg-12'>
        <div className='row pt-20 mb-10'>
          <div className='col-lg-6'>
            <h7 className='uppercase'>DỮ LIỆU HỆ THỐNG</h7>
          </div>
          <div className='col-lg-6'>
            <div className="i-breadcrumb float-right">
            <a className="i-breadcrumb -section">Admin</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <a className="i-breadcrumb -section">Master Data</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <div className="i-breadcrumb">Danh Sách</div>
          </div>
          </div>
        </div>
        <div className='row pb-20 pt-20 bg -white'>
          <div className='col-lg-12'>
            <button class="i-btn -brown" disabled={roleAdmin ? '' : 'disabled'} onClick={()=> showModaData(true, null)}>
              <i class="fas fa-plus-circle mr-1 ml-n2"></i>
              Tạo Dữ Liệu
            </button>
          </div>
        </div>

        <div className='row pb-10 bg -white'>
          <div className='col-lg-9 search'>
            <p className='mr-3'>Hiển Thị</p>
            <div class="i-group">
              <select class="i-search" onChange={e => showSetLimitPage(parseInt(e.currentTarget.value))}>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="50">50</option>
              </select>
            </div>
            <p className='ml-3'></p>
          </div>
          <div className='col-lg-3'>
            <input type="text" class="i-input" placeholder='Tìm Kiếm' onKeyPress={e => search(e)}/>
          </div>
        </div>

        <div className='row bg -white min-table'>
          <div className='col-lg-12'>
            <div class="i-table-wrap" >
              <table class="i-table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Tên</th>
                    <th className='w-10'></th>
                  </tr>
                </thead>
                <tbody>
                  {listDataNation.length > 0 && listDataNation.map((item, index) => (
                    <tr key={ item.id }>
                      <td>{ index + 1 }</td>
                      <td>{ item.name }</td>
                      <td class="text-nowrap">
                        { roleAdmin &&
                          <>
                            <a className='cursor-pointer mr-2' onClick={()=> showModaData(false, item.id) } >
                              <i class="icon icon-edit -sm"></i>
                            </a>
                            <a className='cursor-pointer' onClick={() => deleteMasterData(item.id)}>
                              <i class="icon icon-trash"></i>
                            </a>
                          </>
                        }
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className='i-border-down'></div>
        <div className='row  bg -white pt-10'>
          <div className='col-lg-7'>
            {/* <p>1 den 5 cua 12</p> */}
          </div>
          <div className='col-lg-5 text-right'>
            <ul className="i-pagination">
              { currentPage > 1 &&
                <li className="i-pagination__item -text">
                  <a onClick={()=> showPageChange(currentPage - 1) }>
                    « Prev
                  </a>
                </li>
              }
              { currentPage > 1 &&
                <li className="i-pagination__item">
                  <a onClick={()=> showPageChange(currentPage - 1)}>
                    { currentPage - 1 }
                  </a>
                </li>
              }
              { totalPage > 0 &&
                <li className="i-pagination__item active">{currentPage}</li>
              }
              { totalPage > currentPage &&
                <li className="i-pagination__item">
                  <a onClick={()=> showPageChange(currentPage + 1)}>
                    {currentPage + 1}
                  </a>
                </li>}
              { totalPage > currentPage && <li className="i-pagination__item"><a>…</a></li> }
              { totalPage > currentPage &&
                <li className="i-pagination__item -text">
                  <a onClick={()=>showPageChange(currentPage + 1)}>
                    Next »
                  </a>
                </li>
              }
            </ul>
          </div>
        </div>
      </div>

      <div id='data-areas-popup show' className={`i-popup modal fade ${ isActive ? 'show-modal': '' }`}>
        <div className="modal-dialog">
          <div className="modal-content pt-30 pb-30 pl-30 pr-30">
              <button className="i-popup__close" onClick={()=> showModaData(true, null)}>
                <i className="fas fa-times-circle"></i>
              </button>
              <h2 className="i-popup__title font-weight-bold">Tạo Dữ Liệu Hệ Thống</h2>
              <form onSubmit={ handleSubmit(onSubmit) } onReset={ reset }>
                <div className="mt-20 mb-20">
                  <div className="i-group">
                    <div className="i-label i-text">{`Tên ${title}:`}</div>
                    <input {...register('name')}
                      className={ `i-input ${ errors.name ? '-danger' : '' }`}
                      placeholder={`Nhập Tên ${title}`}
                    />
                    { errors.name && (
                      <p className='error-danger'>{ errors.name?.message }</p>
                    )}
                  </div>
                </div>
                <div className="text-center mb-6">
                  <button type="button" className="i-popup__btn i-btn -border -danger mr-3"
                    onClick={()=> showModaData(true, null)}>
                      Huỷ
                  </button>
                  <button type="submit" className="i-popup__btn i-btn -success -border">Lưu</button>
                </div>
              </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default IndexMaster;
