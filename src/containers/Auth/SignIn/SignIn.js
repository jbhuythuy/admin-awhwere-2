import './signin.scss';
import React from 'react';
import logo_login from '../../../assets/icons/Logo-DSVH.svg';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import ValidateForm from './Validate';
import { useForm } from 'react-hook-form';
import {LoginService} from '../../../services/login.service';
import toast, { Toaster } from 'react-hot-toast';
import Banner from '../../../assets/resources/image/banner.jpeg'


function SignIn(){
  const validateForm = yup.object().shape(ValidateForm(yup));

  const { register, handleSubmit, control,  reset, formState:{ errors } } = useForm({
    resolver: yupResolver(validateForm)
  });

  const onSubmit = (data) => {
    LoginService.login(data)
      .then((item) => {
        if(item.status === true){
          localStorage.setItem('token', item.token);
          localStorage.setItem('currentUser', JSON.stringify(item.user));
          window.location.reload();
        }
        else{
          toast.error('Tài khoản hoặc Mật khẩu không đúng!');
        }
      })
  };

  return(
    <form onSubmit={ handleSubmit(onSubmit) } onReset={ reset }>
      <Toaster
        position="top-center"
        reverseOrder={false}
      />
      <div class="login">
        <div className='login_container justify-content-center row'>
          <div className='col-lg-12'>
            <div class="login__logo">
              <img src={logo_login} alt="logo"/>
            </div>
          </div>
          <div className='col-lg-12'>
            <div className='row'>
            <div className='col-lg-5 login-border-right'>
            <div class="login__input">
              <label class="i-label">Tên Đăng Nhập:<span class="i-text -danger ml-1">*</span></label>
              <input {...register('username')} className={ `i-input ${ errors.username ? '-danger' : '' }`}
                placeholder='Nhập Tên' />
                { errors.username && (
                <p className='error-danger'>{ errors.username?.message }</p>
                )}
            </div>
            <div class="login__input">
              <label class="i-label" type="text">Mật Khẩu:<span class="i-text -danger ml-1">*</span></label>
              <input {...register('password')} type="password" className={ `i-input ${ errors.password ? '-danger' : '' }`}
                placeholder='Nhập Mật Khẩu' />
                { errors.password && (
                <p className='error-danger'>{ errors.password?.message }</p>
                )}
            </div>
            <div class="text-center">
              <button type='submit' class="i-btn -brown login__btn">Đăng Nhập</button>
            </div>
          </div>
          <div className='col-lg-7 floar-right'>
            <p className='font-14 title-support'>HƯỚNG DẪN ĐĂNG NHẬP :</p>
            <p className='ml-2 font-12 text-support'>Các cán bộ, chuyên viên, nhân viên, đối tác,
            có trách nhiệm và chuyên môn được Sở Văn Hoá và Thể Thao Quảng Ninh giao nhiệm vụ được phép truy cập vào hệ thống. Bằng cách nhập vào "Tên đăng nhập"
            là các ký tự định danh được cung cấp bởi quản trị hệ thống,
            không giới hạn ký tự. "Mật Khẩu" là các ký tự bao gồm chữ, số và kí tự đặc biệt, người dùng cần thực hiện thay đổi mật khẩu theo chu kỳ 6 tháng.</p>
            <p className='font-14 title-support'>CẢNH BÁO :</p>
            <p className='ml-2 font-12 mb-0 text-support'>- Các hành vi cố tình đăng nhập trái phép vào hệ thống đều là vi phạm phát luật.</p>
            <p className='ml-2 font-12 mt-0 text-support'>- Tất cả các hành động trên hệ thống cần được sự chấp thuận bởi người có thẩm quyền.</p>
          </div>
            </div>
          </div>

          <div className='col-lg-8 font-12 text-center'>
                  <p>© 2019 Bản quyền thuộc về Sở Văn hóa và Thể thao tỉnh Quảng Ninh
      Cấm sao chép dưới mọi hình thức nếu không có sự chấp thuận bằng văn bản</p>
              </div>
          </div>


      </div>
    </form>
  )
}

export default SignIn;
