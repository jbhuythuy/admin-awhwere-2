
function ValidateForm(yup){
  let objectValidate = {
    username: yup.string().required('Tên không được trống'),
    password: yup.string().required('Mật khẩu không được trống')
  }

  return objectValidate;
};

export default ValidateForm;
