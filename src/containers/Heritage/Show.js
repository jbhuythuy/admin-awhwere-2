import './heritage.scss';
import { Link, useParams} from "react-router-dom";
import React, { useState, useEffect } from 'react';
import { HeritageService } from '../../services/heritage.service';
import { ImageService } from '../../services/Profile/image.service'
import { VideoService } from '../../services/Profile/video.service'
import { DocumentService } from '../../services/Profile/document.service'
import { AudioService } from '../../services/Profile/audio.service'
import { VrService } from '../../services/Profile/vr.service';
import moment from 'moment';
import toast from 'react-hot-toast';
import { confirmAlert } from 'react-confirm-alert';

function Show(){
  const { id } = useParams();
  const [area, setArea] = useState({});
  const [listImages, setListImages] = useState([])
  const [listVideos, setListVideos] = useState([])
  const [listDocs, setListDocs] = useState([])
  const [listSounds, setListSounds] = useState([])
  const [listVr, setListVr] = useState([])
  const [listChild, setListChild] = useState([])
  const [roleAdmin, setRole] = useState(true)

  useEffect(() => {
    HeritageService.getById(id)
      .then(item => {
        if(item.data.length > 0){
          setArea(item.data[0])
          setListChild(item.child_data)
        }
      });

    ImageService.getAll(id).then(item => setListImages(item.data))
    VideoService.getAll(id).then(item => setListVideos(item.data))
    DocumentService.getAll(id).then(item => setListDocs(item.data))
    AudioService.getAll(id).then(item => setListSounds(item.data))
    VrService.getAll(id).then(item => setListVr(item.data))

    let info = JSON.parse(localStorage.getItem('currentUser'))
    let isAdmin = info.role === 'manager'
    setRole(isAdmin)
  }, []);

  function deleteHeritage(id) {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui-confirm'>
            <h1 className='title'>Bạn chắc chứ?</h1>
            <p className='content'>Bạn thực sự muốn xoá dữ liệu?</p>
            <button onClick={onClose}>Không</button>
            <button
              onClick={() => handleClickDelete(onClose, id)}
            >
              Có
            </button>
          </div>
        );
      }
    })
  }

  function handleClickDelete(onClose, id){
    HeritageService.delete(id)
      .then(() => {
        setListChild(items => items.filter(x => x.id !== id));
        toast.success('Xoá hạng mục di sản thành công');
      });

    onClose();
  }

  return(
    <div className='col-md-12 edit-user area-show'>
			<div className='row'>
        <div className='col-lg-12'>
          <div className='row header pt-15 pb-1 mb-10'>
            <div className='col-lg-6'>
              <h7 className='uppercase i-bold'>Xem thông tin di sản</h7>
              <a href={'/heritages'} className='i-btn -primary ml-20'>
                Quay Lại
              </a>
            </div>
            <div className='col-lg-6'>
              <div className="i-breadcrumb float-right">
                <a className="i-breadcrumb -section">Admin</a>
                <i className="i-breadcrumb -icon-divider"></i>
                <a className="i-breadcrumb -section">di sản</a>
                <i className="i-breadcrumb -icon-divider"></i>
                <div className="i-breadcrumb">Thông tin di sản</div>
              </div>
            </div>
          </div>
        </div>
				<div className='col-md-8 card-edit'>
					<h5 className='title pt-2 pb-4'>Thông Tin Chung</h5>
          <div className='row pb-3'>
            <div className='col-lg-5'>
              <p>Tên:</p>
              <p>Loại di sản:</p>
              <p>Xếp hạng di sản:</p>
              <p>Thời gian xếp hạng:</p>
              <p>Địa điểm:</p>
              <p>Quyết định:</p>
              <p>Tọa độ:</p>
            </div>
            <div className='col-lg-7'>
              { Object.keys(area).length >= 0 &&
                <>
                  <p>{ area.name }</p>
                  <p>{ area.kind_name }</p>
                  <p>{ area.rating_name }</p>
                  <p>{ area.date_rating }</p>
                  <p>{ area.place_name }</p>
                  <p>{ area.decision }</p>
                  <p>{ area.coordinate }</p>
                </>
              }
            </div>
          </div>
          { Object.keys(area).length >= 0 && area.introduction !== null && area.introduction !== undefined &&
            <div className='row pb-10 pt-10 justify-content-center introduction'>
              <h4 className='pb-10 col-lg-12'>Giới thiệu chung</h4>
              <div className='col-lg-12 scrollbar pl-30 pr-30'>
                { area.introduction.split('\n').map(str => <p className='mb-0'>{str}</p>) }
              </div>
            </div>
          }
				</div>
				<div className='col-md-4'>
					<div className='row'>
            <div className='user-view ml-4'>
              <h5 className='title pt-2 pl-3 font-14'>Danh sách file</h5>
              <dl className='pl-3'>
                {listImages.length > 0 &&
                  <dt>Ảnh:</dt>
                }
                { listImages && listImages.map(file => (
                  <dd className='pl-3' key={`image-${file.id}`}>{file.name}</dd>
                ))}
                {listVideos.length > 0 &&
                  <dt>Video:</dt>
                }
                { listVideos && listVideos.map(file => (
                  <dd className='pl-3' key={`video-${file.id}`}>{file.name}</dd>
                ))}
                {listDocs.length > 0 &&
                  <dt>Tài Liệu:</dt>
                }
                { listDocs && listDocs.map(file => (
                  <dd className='pl-3' key={`docs-${file.id}`}>{file.name}</dd>
                ))}
                { listSounds.length > 0 &&
                  <dt>Âm Thanh:</dt>
                }
                { listSounds && listSounds.map(file => (
                  <dd className='pl-3' key={`sounds-${file.id}`}>{file.name}</dd>
                ))}
                { listVr.length > 0 &&
                  <dt>3D:</dt>
                }
                { listVr && listVr.map(file => (
                  <dd className='pl-3' key={`vr-${file.id}`}>{file.name}</dd>
                ))}
              </dl>
            </div>
					</div>
				</div>
			</div>
      { listChild.length > 0 &&
        <div className='row mt-20'>
          <p className='font-16'>Hạng Mục Di Sản Liên Quan</p>
          <div className="i-table-wrap w-100" >
            <table className="i-table">
              <thead>
                <tr>
                  <th>#.</th>
                  <th>Tên</th>
                  <th>Ngày Tạo</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                { listChild.map((item, index) => (
                  <tr key={ item.id }>
                    <td>{ index + 1 }</td>
                    <td>{ item.name }</td>
                    <td>{ moment(item.create_date).format('L') }</td>
                    <td class="text-nowrap">
                      { roleAdmin &&
                        <>
                          <a className='cursor-pointer mr-2' href={`/heritages-edit/${ item.id }`} >
                            <i class="icon icon-edit -sm"></i>
                          </a>
                          <a className='cursor-pointer' onClick={() => deleteHeritage(item.id)}>
                            <i class="icon icon-trash"></i>
                          </a>
                        </>
                      }
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      }
		</div>
  );
};

export default Show;
