import './homes.scss';
import React, { useState, useEffect } from 'react';

import intangibleHeritage from '../../assets/resources/image/intangible-heritage.svg';
import physicalHeritage from '../../assets/resources/image/physical-heritage.svg';
import {OverviewService} from '../../services/Overview/overview.service';
import { UserService } from '../../services/user.service';
import { Bar } from "react-chartjs-2";
import {Pie} from "react-chartjs-2";
import {Doughnut} from "react-chartjs-2"
import { Line } from 'react-chartjs-2';
import {
  chartExample1,
  labelMapping
} from '../../variables/charts';

function Home(){
  const [overview, setOverview] = useState({});
  const [listUsers, setListUsers] = useState([]);
  const [bigChartData, setbigChartData] = React.useState("data1");
  const [applicationServer1, setApplicationServer1] = useState({});
  const [applicationServer2, setApplicationServer2] = useState({});
  const [dbServer, setDbServer2] = useState({});
  const setBgChartData = (name) => {
    setbigChartData(name);
  }

  useEffect(() => {
    OverviewService.overview().then(data => {
      setOverview(data);
    });

    UserService.getAll(5, 0).then(item => {
      setListUsers(item.data)
    });

    OverviewService.appServer().then(data => {
      setApplicationServer1(data);
    });
  }, []);


  const configHeritageDataDough = () => {
    let data = {
      labels: [
        labelMapping.province_heritage_count,
        labelMapping.nation_heritage_count,
        labelMapping.special_nation_heritage_count,
        labelMapping.physical_heritage_count,
        labelMapping.intangible_heritage_count,
        labelMapping.world_heritage_count,
        labelMapping.other_heritage_count,
      ],
          datasets: [
        {
          backgroundColor: [
            "#3C9D4E",
            "#C94D6D",
            "#7031AC",
            "#E4BF58",
            "#66CDAA",
            "#4174C9",
            "#FF6347",

          ],
          data: [
            overview.province_heritage_count,
            overview.nation_heritage_count,
            overview.special_nation_heritage_count,
            overview.physical_heritage_count,
            overview.intangible_heritage_count,
            overview.world_heritage_count,
            overview.other_heritage_count,
          ]
        }
      ]
    };
    let option = {
      responsive: true,
        plugins:{
        legend: {
          display: true,
              labels: {
            display: false
          },
          position: "bottom"
        },
        title: {
          display: true,
          text: "Phân bổ loại di sản",
          font: {
            size: 16
          }
        }
      },
    };
    return {
      data,
      option
    }
  }

  const configFileDataPie = () => {
    let data = {
      labels: [
        "3D",
        "VR360",
        "Image",
        "Audio",
        "Videos",
        "Documents"
      ],
      datasets: [
        {
          label: "",
          backgroundColor: [
            "#3e95cd",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850",
            "#E4BF58",
          ],
          data: [
            overview.threed_count,
            overview.vr_count,
            overview.image_count,
            overview.audio_count,
            overview.video_count,
            overview.doc_count
          ]
        }
      ]
    };
    let option = {
      responsive: true,
      plugins: {
        legend: {
          display: true,
          position: "bottom"
        },
        title: {
          display: true,
          text: "Phân bổ loại tập tin",
          font: {
            size: 16
          }
        }
      },
    };
    return {
      data,
      option
    }
  }

  return(
    <div className='homes'>
      <div className='col-lg-12'>
        <div className='row'>
          <div className='col-lg-4'>
            <Doughnut className='count-heritage' data={configHeritageDataDough().data} options={configHeritageDataDough().option}/>
          </div>
          <div className='col-lg-8 '>
            <div className='col-lg-12'>
              <div className='row'>
                <div className='col-lg-12 box-count pt-4 mb-4'>
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Xếp Hạng Di Sản</th>
                        <th scope="col">Số Lượng Di Sản</th>
                        <th scope="col">Cập Nhật</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Di Sản Thế Giới	</td>
                        <td class="count align-bottom font-weight-bold count-text-color">{overview.world_heritage_count}</td>
                        <td>12/8/2019</td>
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>Di sản cấp quốc gia đặc biệt</td>
                        <td class="count align-bottom font-weight-bold count-text-color">{overview.special_nation_heritage_count}</td>
                        <td>12/9/2020</td>
                      </tr>
                      <tr>
                        <th scope="row">3</th>
                        <td>Di sản cấp quốc gia</td>
                        <td class="count align-bottom font-weight-bold count-text-color">{overview.nation_heritage_count}</td>
                        <td>2021</td>
                      </tr>
                      <tr>
                        <th scope="row">4</th>
                        <td>Di sản cấp Tỉnh</td>
                        <td class="count align-bottom font-weight-bold count-text-color">{overview.province_heritage_count}</td>
                        <td>2021</td>
                      </tr>
                      <tr>
                        <th scope="row">5</th>
                        <td>Di sản chưa xếp hạng</td>
                        <td class="count align-bottom font-weight-bold count-text-color">{overview.other_heritage_count}</td>
                        <td>2021</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='col-lg-12'>
       <div className='row'>
        <div className='col-lg-4'>
          <Bar className='system-box'
            data={{
              labels: [
                "Ram",
                "Storage",
                "CPU",
                "Network",
              ],
              datasets: [
                {
                  label: "Máy chủ 1 (Application Server 1) (%)",
                  backgroundColor: [
                    "#8e5ea2",
                    "#3cba9f",
                    "#e8c3b9",
                    "#c45850"
                  ],
                  data: [
                    applicationServer1.usedMemPercentage,
                    20,
                    100-applicationServer1.cpuFreePercent,
                    40,
                    100
                  ]
                }
              ]
            }}
          />
        </div>
        <div className="col-lg-4">
        <Bar className='system-box'
            data={{
              labels: [
                "Ram",
                "Storage",
                "CPU",
                "Network",
              ],
              datasets: [
                {
                  label: "Máy chủ 2 ( Application Server 2 )",
                  backgroundColor: [
                    "rgb(156, 204, 101)",
                    "#8e5ea2",
                    "#3cba9f",
                    "#e8c3b9",
                    "#c45850"
                  ],
                  data: [3872, 5267, 734, 784, 433]
                }
              ]
            }}
            options={{
              legend: { display: false },
              title: {
                display: true,
                text: "Predicted world population (millions) in 2050"
              }
            }}
          />
        </div>
        <div className="col-lg-4">
        <Bar className='system-box'
            data={{
              labels: [
                "RAM",
                "Storage",
                "CPU",
                "Network",
              ],
              datasets: [
                {
                  label: "Máy chủ 3 ( Database Server )",
                  backgroundColor: [
                    "rgb(255, 202, 40)",
                    "#8e5ea2",
                    "#3cba9f",
                    "#e8c3b9",
                    "#c45850"
                  ],
                  data: [3872, 5267, 734, 784, 433]
                }
              ]
            }}
            options={{
              legend: { display: false },
              title: {
                display: true,
                text: "Predicted world population (millions) in 2050"
              }
            }}
          />
        </div>
       </div>
      </div>
      <div className='col-lg-12 mt-4'>
        <div className='row'>
          <div className='col-lg-8'>
            <div className='table-box pt-2 pl-2'>
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Loại Dữ Liệu</th>
                    <th scope="col">Số Lượng Dữ Liệu</th>
                    <th scope="col">Cập Nhật</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Dữ liệu 3D</td>
                    <td class="count align-bottom font-weight-bold count-text-color">{overview.world_heritage_count}</td>
                    <td>12/8/2019</td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>Dữ liệu 3D địa hình</td>
                    <td class="count align-bottom font-weight-bold count-text-color">{overview.special_nation_heritage_count}</td>
                    <td>12/9/2020</td>
                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>Dữ liệu VR360</td>
                    <td class="count align-bottom font-weight-bold count-text-color">{overview.vr_count}</td>
                    <td>2021</td>
                  </tr>
                  <tr>
                    <th scope="row">4</th>
                    <td>Dữ liệu Video</td>
                    <td class="count align-bottom font-weight-bold count-text-color">{overview.province_heritage_count}</td>
                    <td>2021</td>
                  </tr>
                  <tr>
                    <th scope="row">5</th>
                    <td>Dữ liệu hình ảnh</td>
                    <td class="count align-bottom font-weight-bold count-text-color">{overview.image_count}</td>
                    <td>2021</td>
                  </tr>
                  <tr>
                    <th scope="row">6</th>
                    <td>Dữ liệu hồ sơ</td>
                    <td class="count align-bottom font-weight-bold count-text-color">{overview.doc_count}</td>
                    <td>2021</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className='col-lg-4'>
          <div className=''>
          <Pie className='count-heritage' data={configFileDataPie().data} options={configFileDataPie().option} />
          </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;