import '../master_data.scss';
import React from 'react';
import IndexMaster from '../../../components/MasterData/Index'

function Index(){

  return(
    <IndexMaster model_name = {'nations'}/>
  );
};

export default Index;
