
function ValidateForm(yup){
  let objectValidate = {
    name: yup.string().required('Tên không được trống'),
    code: yup.string().required('Loại di sản không được trống')
  }
  return objectValidate;
};

export default ValidateForm;
