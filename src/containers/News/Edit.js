import { BASE_URL } from '../../global/constant';
import '../News/News.scss';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {useParams} from "react-router-dom";
import React, { useState, useEffect } from 'react';
import { ApiService } from '../../services/api.service';
// import {Editor} from "react-draft-wysiwyg";
import {EditorState } from "draft-js";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import Editor from 'ckeditor5-custom-build/build/ckeditor';
import {convertToHTML} from "draft-convert";
import {stateFromHTML} from 'draft-js-import-html';
import {useForm} from "react-hook-form";
import ValidateForm from './Validate';
import {yupResolver} from "@hookform/resolvers/yup";
import * as yup from "yup";
import toast from "react-hot-toast";
import { useHistory } from 'react-router-dom';
import { Modal} from 'react-bootstrap';
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileEncode from 'filepond-plugin-file-encode';
import 'filepond/dist/filepond.min.css';

registerPlugin(FilePondPluginFileValidateType, FilePondPluginFileEncode);

function Edit() {
    const model_name = 'posts';
    const validateForm = yup.object().shape(ValidateForm(yup));
    const history = useHistory();
    const {id} = useParams();
    const [news, setNews] = useState({
        name: "",
        title: "",
        image: "",
        content: ""
    });
    const [editorState, setEditorState] = useState(
        () => EditorState.createEmpty(),
    );
    const  [convertedContent, setConvertedContent] = useState(null);
    const [previewModalShow, setPreviewModalShow] = useState(false);
    const [files, setFiles] = useState([]);
    const handleEditorChange = (state) => {
        setEditorState(state);
        convertContentToHTML();
    }

    const convertContentToHTML = () => {
        let currentContentAsHTML = convertToHTML({
            entityToHTML: (entity, originalText) => {
                if (entity.type === 'image' || entity.type === "IMAGE") {
                    return `<img src="${entity.data.src}"/>`;
                }
                return originalText;
            }
        })(editorState.getCurrentContent());
        setConvertedContent(currentContentAsHTML);
    }

    useEffect(() => {
        ApiService.getById(model_name, id)
            .then(obj => {
                if(obj.data !== undefined){
                    setNews(obj.data);
                    let contentState = stateFromHTML(obj.data.content);
                    let editorState = EditorState.createWithContent(
                        contentState
                    );
                    setEditorState(editorState);
                }
            });
    }, []);

    const {
        register, handleSubmit, setValue, control,
        reset, formState:{ errors }, clearErrors
    } = useForm({
        resolver: yupResolver(validateForm)
    });

    function onSubmit(data, convertedContent) {
        var content = { content: convertedContent };
        let info = JSON.parse(localStorage.getItem('currentUser'));
        let image = createFile(files);
        var create_uid = {create_uid: parseInt(info.id)};
        data = {...data, ...content, ...image};
        data = {...data, ...create_uid};
        console.log(data);
        ApiService.update(model_name, id, data)
            .then(() => {
                toast.success('Cập nhật data thành công')
                history.push('/news-manager');
        });
    }

    function handleOpenPreviewModal(event) {
        event.preventDefault();
        if(convertedContent === null) convertContentToHTML();
        setPreviewModalShow(true);
    }

    function handleClosePreviewModal() {
        setPreviewModalShow(false);
    }

    function createFile(files){
        let data = {};
        if(files.length > 0) {
            data = {
                image: {
                    type: files[0].fileType,
                    size: files[0].fileSize,
                    name: files[0].filename,
                    base64: files[0].getFileEncodeBase64String()
                }
            }
        }
        return data;
    }

    function handleUpdateFile(files){
        setFiles(files);
    }
    return (
        <div className='col-md-12 edit-user'>
            <form onSubmit={handleSubmit(data => onSubmit(data, convertedContent))}  onReset={ reset }>
                <div className='row'>
                    <div className='col-md-12 card-edit'>
                        <div className='row mb-2 pt-2 bg-title'>
							<div className='col-lg-6'>
								<h7 className='uppercase'>CHỈNH SỬA BÀI VIẾT</h7>
								<a href={'/news-manager'} className='i-btn -primary ml-20 mb-1'>
									Quay Lại
								</a>
							</div>
							<div className='col-lg-6 '>
								<div className='i-breadcrumb float-right'>
									<a className='i-breadcrumb -section'>Admin</a>
									<i className='i-breadcrumb -icon-divider'></i>
									<a className='i-breadcrumb -section'>bài viết</a>
									<i className='i-breadcrumb -icon-divider'></i>
									<div className='i-breadcrumb'>chỉnh sửa bài viết</div>
								</div>
							</div>
						</div>
                        <div className='row'>
                            <div className='input-container col-md-6'>
                                <label className='label ml-2'>Tiêu đề bài viết</label>
                                <input
                                    {...register('name')}
                                    className={ `address-box`}
                                    defaultValue={news.name ? news.name : ""}/>
                                { errors.name && (
                                    <p className='error-danger'>{ errors.name?.message }</p>
                                )}
                            </div>
                            <div className='input-container col-md-6'>
                                <label className='label ml-2'>Mô tả bài viết</label>
                                <input {...register('title')}
                                       className={ `address-box`}
                                       defaultValue={news.title ? news.title : ""}/>
                                { errors.title && (
                                    <p className='error-danger'>{ errors.title?.message }</p>
                                )}
                            </div>
                            <br/>
                            <div className="input-container col-md-4">
                                <label className="label ml-2">Tải lên ảnh đại diện mới: </label>{"  "}
                                <FilePond
                                    files={files}
                                    allowMultiple={false}
                                    onupdatefiles={handleUpdateFile}
                                    labelIdle='Tải File'
                                    allowFileTypeValidation={true}
                                    acceptedFileTypes= {['image/*']}
                                    labelFileTypeNotAllowed='File không hợp lệ'
                                    fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file ảnh'
                                    allowFileEncode={true}
                                />
                            </div>
                            <div className="input-container col-md-8">
                                <label className="label ml-2">Ảnh đại diện hiện tại</label>
                                <div>
                                    <img src={BASE_URL+"/file/"+news.image} style={{maxWidth:"500px"}}/>
                                </div>
                            </div>
                            <div className='box-editer col-md-12 mt-4  pb-4'>
                                <label className='label ml-2'>Nội dung bài viết</label>
                                <div style={{ border: "2px solid rgba(29, 37, 59, 0.3)", padding: '2px', minHeight: '300px', border_radius: "20px" }}>
                                    <CKEditor
											editor={ Editor }
											data={news.content}
											onChange =  { ( event, editor ) => { 
												setConvertedContent(editor.getData());
											 }}
										/>
                                    {/* <div className="preview" dangerouslySetInnerHTML={createMarkup(convertedContent)}></div> */}
                                </div>
                                { errors.content && (
                                    <p className='error-danger'>{ errors.content?.message }</p>
                                )}
                            </div>
                            <div className='col-md-12 mb-4'>
                                <div className='row justify-content-center'>
                                    <button className={`i-btn -brown col-md-2`} onClick={ handleOpenPreviewModal }>
										Xem thử
									</button>
                                    <button className='i-btn -success col-md-2 ml-4'>
                                        Cập nhật
									</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <Modal className='preview-new' size="xl" centered fullscreen="xl-down" show={previewModalShow} style={{top: '-45%'}}>
                <Modal.Header>
                    <Modal.Title>Xem Thử</Modal.Title>
                    <button className="i-popup__close pt-0" onClick={handleClosePreviewModal}>
                        <i className="fas fa-times-circle"></i>
                    </button>
                </Modal.Header>
                <Modal.Body>
                    <div dangerouslySetInnerHTML={{__html: convertedContent}} />
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default Edit;