import { BASE_URL } from '../../global/constant';
import '../News/News.scss';
import { Link } from "react-router-dom";
import React, { useState, useEffect } from 'react';
import { ApiService } from '../../services/api.service';
import toast from 'react-hot-toast';
import moment from 'moment';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

function News() {
  const [listNews, setlistNews] = useState([]);
  const [currentPage, setcurrentPage] = useState(1);
  const [limitPage, setLimitPage] = useState(10);
  const [offsetPage, setoffsetPage] = useState(0)
  const [totalPage, settotalPage] = useState(0)
  const [isSearch, setSearch] = useState(false);
  const [valueSearch, setvalueSearch] = useState('');
  const model_name = 'posts';
  const [roleAdmin, setRole] = useState(true)
  const newsFields = ['name', 'name_eng', 'title', 'title_eng', 'image', 'create_date', 'write_date', 'create_uid'];

  useEffect(() => {
    ApiService.getAllNews(limitPage, offsetPage, newsFields).then(item => {
      setlistNews(item.data)
      settotalPage(Math.ceil(parseInt(item.total) / limitPage))
    })

    let info = JSON.parse(localStorage.getItem('currentUser'))
    let isAdmin = info.role === 'manager'
    setRole(isAdmin)
  }, []);

  function showSetLimitPage(value){
    setLimitPage(value)
    ApiService.getAllNews(value, 0, newsFields).then(item => {
      setlistNews(item.data)
      settotalPage(Math.ceil(parseInt(item.total) / value))
    })
  }

  function search(event) {
    if (event.charCode === 13) {
      if(event.target.value == ''){
        setSearch(!isSearch)

        ApiService.getAllNews(limitPage, 0, newsFields).then(item => {
          setlistNews(item.data)
          settotalPage(Math.ceil(parseInt(item.total) / limitPage))
        })
      }
      else {
        setSearch(!isSearch)
        setvalueSearch(event.target.value)

        ApiService.searchNews(`table.c.name.like(('%${ event.target.value }%'))`, limitPage, 0)
          .then(item => {
            setlistNews(item.data)
            settotalPage(Math.ceil(parseInt(item.total) / limitPage))
          })
      }
    }
  }

  function deleteNews(id) {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui-confirm'>
            <h1 className='title'>Bạn chắc chứ?</h1>
            <p className='content'>Bạn thực sự muốn xoá dữ liệu?</p>
            <button onClick={onClose}>Không</button>
            <button
              onClick={() => handleClickDelete(onClose, id)}
            >
              Có
            </button>
          </div>
        );
      }
    })
  }

  function handleClickDelete(onClose, id){
    ApiService.delete(model_name, id)
      .then(() => {
        setlistNews(items => items.filter(item => item.id !== id));
        toast.success('Xoá data thành công');
      });

    onClose();
  }

  function showPageChange(value) {
    const offset = limitPage * (value -1)
    setcurrentPage(value)
    setoffsetPage(offset)

    if(isSearch) {
      ApiService.searchNews(`table.c.name.like(('%${ valueSearch }%'))`, limitPage, offset)
        .then(item => {
          setlistNews(item.data)
        })
    }
    else {
      ApiService.getAllNews(limitPage, offset).then(item => {
        setlistNews(item.data)
      })
    }
  }

  return (
    <div className='row pl-0 pr-0 bg -blue-light area'>
      <div className='col-lg-12'>
        <div className='row pt-15 mb-10'>
          <div className='col-lg-6'>
            <p className='uppercase font-13 mb-0'>QUẢN LÝ TIN TỨC</p>
          </div>
          <div className='col-lg-6'>
            <div className="i-breadcrumb float-right">
            <a className="i-breadcrumb -section">Admin</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <a className="i-breadcrumb -section">Quản Lý Tin Tức</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <div className="i-breadcrumb">Danh Sách</div>
          </div>
          </div>
        </div>
        <div className='row pb-20 pt-20 bg -white'>
          <div className='col-lg-12'>
            <Link to={`/add-news`} className={roleAdmin ? '' : 'bt-disabled'}>
              <button class="i-btn -brown" disabled={roleAdmin ? '' : 'disabled'}>
                <i class="fas fa-plus-circle mr-1 ml-n2 "></i>
                Viết Bài Mới
              </button>
            </Link>
          </div>
        </div>

        <div className='row pb-10 bg -white'>
          <div className='col-lg-9 search'>
            <p className='mr-3'>Hiển Thị</p>
            <div class="i-group">
              <select class="i-search" onChange={e => showSetLimitPage(parseInt(e.currentTarget.value))}>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="50">50</option>
              </select>
            </div>
            <p className='ml-3'></p>
          </div>
          <div className='col-lg-3'>
            <input type="text" class="i-input" placeholder='Tìm Kiếm' onKeyPress={e => search(e)}/>
          </div>
        </div>
        <div className='row bg-white min-table'>
          <div className='col-lg-12'>
            <div class="i-table-wrap" >
              <table class="i-table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Tiêu đề</th>
                    <th>Ảnh đại diện</th>
                    <th>Mô tả</th>
                    <th>Tác giả</th>
                    <th>Ngày xuất bản</th>
                    <th>Ngày cập nhật</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {listNews.length > 0 && listNews.map((item, index) => (
                    <tr key={ item.id }>
                      <td>{ index + 1 }</td>
                      <td>{item.name}</td>
                      <td>
                        {
                            <img src={BASE_URL+"/file/"+item.image} style={{maxHeight:"5rem"}}/>
                        }
                      </td>
                      <td>{item.title}</td>
                      <td>{item.create_uname}</td>
                      <td>{ moment(item.create_date).format('L') }</td>
                      <td>{ moment(item.write_date).format('L') }</td>
                      <td class="text-nowrap">
                        <a className="mr-2" href={`/news-show/${ item.id }`} >
                          <i className="icon icon-eye"></i>
                        </a>
                        { roleAdmin &&
                          <>
                            <a className="mr-2" href={`/news-edit/${ item.id }`}>
                              <i className="icon icon-edit -sm"></i>
                            </a>
                            <a className='cursor-pointer' onClick={() => deleteNews(item.id)}>
                              <i class="icon icon-trash"></i>
                            </a>
                          </>
                        }
                      </td>
                    </tr>
                  ))}
                </tbody>

              </table>
            </div>
          </div>
        </div>
        <div className='i-border-down'></div>
        <div className='row  bg -white pt-10'>
          <div className='col-lg-7'>
          </div>
          <div className='col-lg-5 text-right'>
            <ul className="i-pagination">
              { currentPage > 1 &&
                <li className="i-pagination__item -text">
                  <a onClick={()=> showPageChange(currentPage - 1) }>
                    « Prev
                  </a>
                </li>
              }
              { currentPage > 1 &&
                <li className="i-pagination__item">
                  <a onClick={()=> showPageChange(currentPage - 1)}>
                    { currentPage - 1 }
                  </a>
                </li>
              }
              { totalPage > 0 &&
                <li className="i-pagination__item active">{currentPage}</li>
              }
              { totalPage > currentPage &&
                <li className="i-pagination__item">
                  <a onClick={()=> showPageChange(currentPage + 1)}>
                    {currentPage + 1}
                  </a>
                </li>}
              { totalPage > currentPage && <li className="i-pagination__item"><a>…</a></li> }
              { totalPage > currentPage &&
                <li className="i-pagination__item -text">
                  <a onClick={()=>showPageChange(currentPage + 1)}>
                    Next »
                  </a>
                </li>
              }
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default News
