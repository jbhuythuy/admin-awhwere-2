import { BASE_URL } from '../../global/constant';
import '../News/News.scss';
import {Link, useParams} from "react-router-dom";
import React, { useState, useEffect } from 'react';
import { ApiService } from '../../services/api.service';
const model_name = 'posts';

function Show() {
    const {id} = useParams();
    const [news, setNews] = useState({});
    useEffect(() => {
      ApiService.getById(model_name, id)
        .then(obj => {
          if(obj.data !== undefined){
            setNews(obj.data);
          }
        });
    }, []);
    return (
      <div className='col-md-12 edit-user'>
          <form>
              <div className='row'>
                  <div className='col-md-12 card-edit'>
                      <div className='row'>
                          <div className='input-container col-md-6'>
                              <label className='label ml-2'>Tiêu đề bài viết</label>
                              <input className={ `address-box`} defaultValue={news.name}/>
                          </div>
                          <div className='input-container col-md-6'>
                              <label className='label ml-2'>Mô tả bài viết</label>
                              <input className={ `address-box`} defaultValue={news.title}/>
                          </div>
                          <div className='box-editer col-md-12 mt-4  pb-4'>
                              <label className='label ml-2'>Ảnh đại diện</label>
                              <div>
                                  <img src={BASE_URL+"/file/"+news.image} style={{maxWidth:"500px"}}/>
                              </div>
                          </div>
                          <div className='box-editer col-md-12 mt-4  pb-4'>
                              <label className='label ml-2'>Nội dung bài viết</label>
                              <div style={{ border: "2px solid rgba(29, 37, 59, 0.3)", padding: '2px', minHeight: '300px', border_radius: "20px" }}>
                                  <div dangerouslySetInnerHTML={{__html: news.content}} />
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </form>
      </div>
    );
}

export default Show;