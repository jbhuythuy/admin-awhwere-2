function ValidateForm(yup){
  let objectValidate = {
      name: yup.string().required('Tiêu đề không được trống'),
      title: yup.string().required('Mô tả không được trống hoặc quá 150 ký tự').max(150),
  }
  return objectValidate;
};

export default ValidateForm;
