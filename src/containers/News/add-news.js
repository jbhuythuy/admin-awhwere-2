import './News.scss';
import React, { useState } from "react";
import { EditorState } from 'draft-js';
import { convertToHTML } from 'draft-convert';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import Editor from 'ckeditor5-custom-build/build/ckeditor';
import DOMPurify from 'dompurify';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { ApiService } from '../../services/api.service';
import toast from 'react-hot-toast';
import ValidateForm from './Validate';
import { useHistory } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileEncode from 'filepond-plugin-file-encode';
import 'filepond/dist/filepond.min.css';
registerPlugin(FilePondPluginFileValidateType, FilePondPluginFileEncode);

function Edituser () {
	const validateForm = yup.object().shape(ValidateForm(yup));
	const model_name = 'posts';
	const history = useHistory();
	const [editorState, setEditorState] = useState(
    	() => EditorState.createEmpty(),
  	);
	const  [convertedContent, setConvertedContent] = useState(null);
	const [thumbnail, setThumbnail] = useState("");
	const [files, setFiles] = useState([]);
	const [previewModalShow, setPreviewModalShow] = useState(false);
	const handleEditorChange = (state) => {
		setEditorState(state);
		convertContentToHTML();
	}

	const onEditorChange = (state) => {
		// console.log(state.editor.getData());
		setConvertedContent(state.editor.getData());
		// setEditorState(state);
		console.log(state.editor.getData());
		// convertContentToHTML();
	}

	const onFileUploadRequestEditor = (state) => {
		console.log("s123", state.editor);
		let currentContentAsHTML = convertToHTML({
			entityToHTML: (entity, originalText) => {
				if (entity.type === 'image' || entity.type === "IMAGE") {
					return `<img src="${entity.data.src}"/>`;
				}
				return originalText;
			}
		})(editorState.getCurrentContent());
		console.log('ccccccc', currentContentAsHTML)
	}



  const convertContentToHTML = () => {
	  let currentContentAsHTML = convertToHTML({
		  entityToHTML: (entity, originalText) => {
			  if (entity.type === 'image' || entity.type === "IMAGE") {
				  return `<img src="${entity.data.src}"/>`;
			  }
			  return originalText;
		  }
	  })(editorState.getCurrentContent());
	//   console.log('curret', currentContentAsHTML);
	  setConvertedContent(currentContentAsHTML);
  }

	const {
    register, handleSubmit, setValue, control,
    reset, formState:{ errors }, clearErrors
  	} = useForm({
      resolver: yupResolver(validateForm)
    });

	function createFile(files){
		let data = {};
		if(files.length > 0){
			data = {
				image: {
					type: files[0].fileType,
					size: files[0].fileSize,
					name: files[0].filename,
					base64: files[0].getFileEncodeBase64String()
				}
			}
		}
		return data;
	}

	function handleUpdateFile(files){
		setFiles(files);
	}

  function onSubmit(data, convertedContent) {
	  var content = { content: convertedContent };
	  let info = JSON.parse(localStorage.getItem('currentUser'));
	  var create_uid = {create_uid: parseInt(info.id)};
	  let image = createFile(files);
	  data = {...data, ...content, ...image}
	  data = {...data, ...create_uid}

	  ApiService.create(model_name, data)
      .then(() => {
        toast.success('Tạo data thành công')
				history.push('/news-manager');
      })
  }

	function handleOpenPreviewModal(event) {
		event.preventDefault();
		console.log(convertedContent);
		if (convertedContent === null) convertContentToHTML();
		setPreviewModalShow(true);
	}
	function handleClosePreviewModal() {
		setPreviewModalShow(false);
	}

	function uploadImageCallBack(file) {
		return new Promise(
			(resolve, reject) => {
				const reader = new FileReader();
				reader.onload = e => resolve({ data: { link: e.target.result } });
				reader.onerror = e => reject(e);
				reader.readAsDataURL(file);
			});
	}

	return (
		<div className='col-md-12 edit-user'>
			<form onSubmit={handleSubmit(data => onSubmit(data, convertedContent))}  onReset={ reset }>
				<div className='row'>
					<div className='col-md-12 card-edit'>
						<div className='row mb-2 pt-2 bg-title'>
							<div className='col-lg-6'>
								<h7 className='uppercase'>TẠO BÀI VIẾT MỚI</h7>
								<a href={'/news-manager'} className='i-btn -primary ml-20 mb-1'>
									Quay Lại
								</a>
							</div>
							<div className='col-lg-6 '>
								<div className='i-breadcrumb float-right'>
									<a className='i-breadcrumb -section'>Admin</a>
									<i className='i-breadcrumb -icon-divider'></i>
									<a className='i-breadcrumb -section'>bài viết</a>
									<i className='i-breadcrumb -icon-divider'></i>
									<div className='i-breadcrumb'>tạo bài viết</div>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='input-container col-md-6'>
								<label className='label ml-2'>Tiêu đề bài viết</label>
								<input {...register('name')}
									className={ `address-box`}
									placeholder='Nhập vào tiêu đề bài viết'
								/>
								{ errors.name && (
									<p className='error-danger'>{ errors.name?.message }</p>
								)}
							</div>
							<div className='input-container col-md-6'>
								<label className='label ml-2'>Mô tả bài viết</label>
								<input {...register('title')}
									className={ `address-box`}
									placeholder='Nội dung mô tả về bài viết không quá 150 ký tự'
								/>
								{ errors.title && (
									<p className='error-danger'>{ errors.title?.message }</p>
								)}
							</div>
							<div className="input-container col-sm-2">
								<label className="label ml-2">Ảnh đại diện: </label>{"  "}
								<FilePond
									files={files}
									allowMultiple={false}
									onupdatefiles={handleUpdateFile}
									labelIdle='Tải file'
									allowFileTypeValidation={true}
									acceptedFileTypes= {['image/*']}
									labelFileTypeNotAllowed='File không hợp lệ'
									fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file ảnh'
									allowFileEncode={true}
								/>
							</div>
							<div className='box-editer col-md-12 mt-4  pb-4'>
								<label className='label ml-2'>Nội dung bài viết</label>
									<div style={{ border: "2px solid rgba(29, 37, 59, 0.3)", padding: '2px', border_radius: "20px" }}>
										<CKEditor
											editor={ Editor }
											data="<p></p>"
											onChange =  { ( event, editor ) => { 
												setConvertedContent(editor.getData());
											 }}
										/>
										
									</div>
							</div>
							<div className='col-md-12 mb-4'>
								<div className='row justify-content-center'>
									<button className={`i-btn -brown col-md-2`} onClick={ handleOpenPreviewModal }>
										Xem thử
									</button>
                  <button className='i-btn -success col-md-2 ml-4'>
										Đăng bài
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>

			<Modal className='preview-new' size="xl" centered fullscreen="xl-down" show={previewModalShow} style={{top: '-45%'}}>
				<Modal.Header>
					<Modal.Title>Xem Thử</Modal.Title>
					<button className="i-popup__close pt-0" onClick={handleClosePreviewModal}>
						<i className="fas fa-times-circle"></i>
					</button>
				</Modal.Header>
				<Modal.Body>
					<div dangerouslySetInnerHTML={{__html: convertedContent}} />
				</Modal.Body>
			</Modal>
		</div>
	);
};

export default Edituser;
