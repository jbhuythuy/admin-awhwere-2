import './list_profile.scss';

function Detail(){
  return(
    <div className='row pl-0 pr-0 bg -blue-light detail-profile'>
      <div className='col-lg-12 pl-0'>
        <div className='row pt-20 mb-10 justify-content-center'>
          <div className='col-lg-5 pl-0'>
            <h7 className='uppercase'>hồ sơ</h7>
          </div>
          <div className='col-lg-6'>
            <div className="i-breadcrumb float-right">
            <a className="i-breadcrumb -section">Admin</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <a className="i-breadcrumb -section">hồ sơ</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <div className="i-breadcrumb">Danh Sách</div>
          </div>
          </div>
        </div>
        <div className='row justify-content-center'>
          <div className='col-lg-11 bg -white min-height'>
            <div className='row'>
              <div className='col-lg-5 pb-20'>
                <div className='pt-20 pb-10'>
                  <h7>TÊN DI SẢN</h7>
                </div>
                <div className='scrollbar'>
                  <div className='file-vr'>
                    <div>
                      <i class="icon icon-360 mt-2"/>
                    </div>
                    <div className='pl-3'>
                      <p className='mb-0 pt-2 file-vr__content'>Đền thờ Vua Lý Anh Tông và Động Đông...</p>
                      <p>Đông anh</p>
                    </div>
                  </div>
                  <div className='file-vr'>
                    <div>
                      <i class="icon icon-360 mt-2"/>
                    </div>
                    <div className='pl-3'>
                      <p className='mb-0 pt-2 file-vr__content'>Đền thờ Vua Lý Anh Tông và Động Đông...</p>
                      <p>Đông anh</p>
                    </div>
                  </div>
                  <div className='file-vr'>
                    <div>
                      <i class="icon icon-360"/>
                    </div>
                    <div className='pl-3'>
                      <p className='mb-0 pt-2 file-vr__content'>Đền thờ Vua Lý Anh Tông và Động Đông...</p>
                      <p>Đông anh</p>
                    </div>
                  </div>
                  <div className='file-vr'>
                    <div>
                      <i class="icon icon-360"/>
                    </div>
                    <div className='pl-3'>
                      <p className='mb-0 pt-2 file-vr__content'>Đền thờ Vua Lý Anh Tông và Động Đông...</p>
                      <p>Đông anh</p>
                    </div>
                  </div>
                  <div className='file-vr'>
                    <div>
                      <i class="icon icon-360"/>
                    </div>
                    <div className='pl-3'>
                      <p className='mb-0 pt-2 file-vr__content'>Đền thờ Vua Lý Anh Tông và Động Đông...</p>
                      <p>Đông anh</p>
                    </div>
                  </div>
                  <div className='file-vr'>
                    <div>
                      <i class="icon icon-360"/>
                    </div>
                    <div className='pl-3'>
                      <p className='mb-0 pt-2 file-vr__content'>Đền thờ Vua Lý Anh Tông và Động Đông...</p>
                      <p>Đông anh</p>
                    </div>
                  </div>
                  <div className='file-vr'>
                    <div>
                      <i class="icon icon-360"/>
                    </div>
                    <div className='pl-3'>
                      <p className='mb-0 pt-2 file-vr__content'>Đền thờ Vua Lý Anh Tông và Động Đông...</p>
                      <p>Đông anh</p>
                    </div>
                  </div>
                  <div className='file-vr'>
                    <div>
                      <i class="icon icon-360"/>
                    </div>
                    <div className='pl-3'>
                      <p className='mb-0 pt-2 file-vr__content'>Đền thờ Vua Lý Anh Tông và Động Đông...</p>
                      <p>Đông anh</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className='col-lg-7 pb-20'>
                <div className='pt-20 pb-10'>
                  <h7>DỮ LIỆU</h7>
                </div>
                <div class="i-table-wrap scrollbar">
                  <table class="i-table">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Tên File</th>
                        <th>Dung Lượng</th>
                        <th>Ngày Tạo</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Ten file di san</td>
                        <td>5M</td>
                        <td>20/04/2021</td>
                        <td class="text-nowrap">
                          <a href="#" >
                            <i class="icon icon-trash"></i>
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  );
};

export default Detail;
