import { BASE_URL } from '../../../global/constant';
import '../list_profile.scss';
import 'filepond/dist/filepond.min.css';
import React, { useState, useEffect, useRef } from 'react';
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileEncode from 'filepond-plugin-file-encode';
import toast from 'react-hot-toast';
import { ProfileService } from '../../../services/profile.service'
import { ImageService } from '../../../services/Profile/image.service'
import moment from 'moment';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

registerPlugin(FilePondPluginFileValidateType, FilePondPluginFileEncode);

function Index(){
  const [files, setFiles] = useState([])
  const [listfiles, setListFiles] = useState([])
  const [heritage, setHeritage] = useState({})
  const inputEl = useRef(null);
  const [listHeritage, setListHeritage] = useState([])
  const [roleAdmin, setRole] = useState(true)

  function createFile(files, heritage){

    let params_file =  files.map(file => ({
      type: file.fileType,
      size: file.fileSize,
      name: file.filename,
      base64: file.getFileEncodeBase64String()
    }))

    let data = {
      heritage_id: heritage.id,
      values: {
        images: params_file
      }
    }

    return ImageService.create(data)
      .then((item) => {
        toast.success('Tạo file thành công');
        setFiles([]);
        ImageService.getAll(heritage.id).then(item => setListFiles(item.data))
      })
  }

  function showFile(heritage_id, heritage_name){
    ImageService.getAll(heritage_id).then(item => setListFiles(item.data))
    setHeritage({id: heritage_id, name: heritage_name})
  }

  function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(0) + ' ' + sizes[i];
  }

  function viewFileImg(url) {
    let link = `${BASE_URL}/file/${url}`;
    window.open(link, '_blank').focus();
  }

  async function downloadFileImg(url, name){

    const image = await fetch(`${BASE_URL}/file/${url}`);
    const imageBlog = await image.blob();
    const imageURL = URL.createObjectURL(imageBlog);

    const link = document.createElement('a');
    link.href = imageURL;
    link.download = name;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  function deleteFileImg(id){
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui-confirm'>
            <h1 className='title'>Bạn chắc chứ?</h1>
            <p className='content'>Bạn thực sự muốn xoá dữ liệu?</p>
            <button onClick={onClose}>Không</button>
            <button
              onClick={() => handleClickDelete(onClose, id)}
            >
              Có
            </button>
          </div>
        );
      }
    })
  }

  function handleClickDelete(onClose, id){
    ImageService.delete('heritage_images', id)
      .then(() => {
        setListFiles(items => items.filter(item => item.id !== id));
        toast.success('Xoá file thành công');
      });

    onClose();
  }

  useEffect(() => {
    ProfileService.getAll('images').then(item => setListHeritage(item.data))

    let info = JSON.parse(localStorage.getItem('currentUser'))
    let isAdmin = info.role === 'manager'
    setRole(isAdmin)
  }, []);

  return(
    <div className='row pl-0 pr-0 bg -blue-light detail-profile'>
      <div className='col-lg-12'>
        <div className='row pt-15 mb-10'>
          <div className='col-lg-6 pl-0'>
            <a href={'/list-profile'} className='i-btn -primary ml-20'>
              Quay Lại
            </a>
            { roleAdmin &&
              <>
                { Object.keys(heritage).length > 0 &&
                  <button type='submit' className='i-btn -success ml-20'
                    onClick={() => createFile(files, heritage) }>Lưu</button>
                }
              </>
            }
          </div>
          <div className='col-lg-6'>
            <div className="i-breadcrumb float-right">
            <a className="i-breadcrumb -section">Admin</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <a className="i-breadcrumb -section">hồ sơ</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <div className="i-breadcrumb">Danh Sách</div>
          </div>
          </div>
        </div>
        <div className='row justify-content-center'>
          { roleAdmin &&
            <>
              { Object.keys(heritage).length > 0 &&
                <div className='col-lg-12  pr-0 bg -white'>
                  <FilePond
                    files={files}
                    allowMultiple={true}
                    onupdatefiles={setFiles}
                    labelIdle='Tải File'
                    allowFileTypeValidation={true}
                    acceptedFileTypes= {['image/*']}
                    labelFileTypeNotAllowed='File không hợp lệ'
                    fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file ảnh'
                    allowFileEncode={true}
                  />
                </div>
              }
            </>
          }
          <div className='col-lg-12 bg -white min-height'>
            <div className='row'>
              <div className='col-lg-5 pb-20'>
                <div className='pt-20 pb-10'>
                  <h7 className='i-bold'>TÊN DI SẢN</h7>
                </div>
                <div className='col-lg-12 pl-0 pb-2'>
                  {/* <input type="text" class="i-input" placeholder='Tìm Kiếm' /> */}
                </div>
                <div className='scrollbar'>
                  {listHeritage && listHeritage.map(item => (
                    <div className={ `file-vr` } key={ item.id }
                      onClick={ ()=> showFile(item.id, item.name) } ref={inputEl}>
                      <div>
                        <i class="icon icon-hinhanh mt-2"/>
                      </div>
                      <div className='pl-3'>
                        <p className='mb-0 pt-2 file-vr__content'>{ item.name }</p>
                        <p>{ item.place_name }</p>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              <div className='col-lg-7 pb-20'>
                <div className='pt-20 pb-10'>
                  <h7 className='i-bold uppercase'>DỮ LIỆU: { heritage.name }</h7>
                </div>
                <div class="i-table-wrap scrollbar">
                  <table class="i-table">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Tên File</th>
                        <th>Dung Lượng</th>
                        <th>Ngày Tạo</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      { listfiles && listfiles.map((file, index) => (
                        <tr key={ file.id }>
                          <td>{ index + 1 }</td>
                          <td>{ file.name }</td>
                          <td>{ bytesToSize(file.size)}</td>
                          <td>{ moment(file.create_date).format('L') }</td>
                          <td className="text-nowrap">
                            { roleAdmin &&
                              <>
                                <a className='cursor-pointer' onClick={() => viewFileImg(file.url)} >
                                  <i className="icon icon-eye"></i>
                                </a>&nbsp;
                                <a className='cursor-pointer' onClick={() => downloadFileImg(file.url, file.name)} >
                                  <i className="fas fa-download"></i>
                                </a>&nbsp;
                                <a className='cursor-pointer' onClick={() => deleteFileImg(file.id)} >
                                  <i className="icon icon-trash"></i>
                                </a>
                              </>
                            }
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Index;
