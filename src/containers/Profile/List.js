import './list_profile.scss';
import { useHistory } from "react-router-dom";

function List(){
  let history = useHistory();

  function showProfile(ele){
    history.push(`/list-${ ele }`);
  }

  return(
    <div className='row pl-0 pr-0 bg -blue-light list-profile'>
      <div className='col-lg-12'>
        <div className='row pt-15 mb-10'>
          <div className='col-lg-6'>
            <p className='uppercase font-13 mb-0'>HỒ SƠ</p>
          </div>
          <div className='col-lg-6'>
            <div className="i-breadcrumb float-right">
              <a className="i-breadcrumb -section">Admin</a>
              <i className="i-breadcrumb -icon-divider"></i>
              <a className="i-breadcrumb -section">hồ sơ</a>
              <i className="i-breadcrumb -icon-divider"></i>
              <div className="i-breadcrumb">Danh Sách</div>
            </div>
          </div>
        </div>
      </div>
      <div className='col-lg-12 bg -white min-height'>
        <div className='row pt-30'>
          <div className='col-lg-3 folder mr-20 ml-25' onDoubleClick={()=> showProfile('vr')}>
            <div className='item bg -blue-light mt-15'>
              <i class="fas fa-folder-open pt-13 pl-13"></i>
            </div>
            <div className='mt-25 pl-20'>DỮ LIỆU VR</div>
          </div>
          <div className='col-lg-3 folder mr-20' onDoubleClick={()=> showProfile('vr3d')}>
            <div className='item bg -blue-light mt-15'>
              <i class="fas fa-folder-open pt-13 pl-13"></i>
            </div>
            <div className='mt-25 pl-20'>DỮ LIỆU 3D</div>
          </div>
          <div className='col-lg-3 folder' onDoubleClick={()=> showProfile('images')}>
            <div className='item bg -blue-light mt-15'>
              <i class="fas fa-folder-open pt-13 pl-13"></i>
            </div>
            <div className='mt-25 pl-20'>DỮ LIỆU ẢNH</div>
          </div>
        </div>

        <div className='row pt-30'>
          <div className='col-lg-3 folder mr-20 ml-25' onDoubleClick={()=> showProfile('videos')}>
            <div className='item bg -blue-light mt-15'>
              <i class="fas fa-folder-open pt-13 pl-13"></i>
            </div>
            <div className='mt-25 pl-20'>DỮ LIỆU VIDEO</div>
          </div>
          <div className='col-lg-3 folder mr-20' onDoubleClick={()=> showProfile('documents')}>
            <div className='item bg -blue-light mt-15'>
              <i class="fas fa-folder-open pt-13 pl-13"></i>
            </div>
            <div className='mt-25 pl-20'>TÀI LIỆU</div>
          </div>
          <div className='col-lg-3 folder' onDoubleClick={()=> showProfile('sounds')}>
            <div className='item bg -blue-light mt-15'>
              <i class="fas fa-folder-open pt-13 pl-13"></i>
            </div>
            <div className='mt-25 pl-20'>DỮ LIỆU ÂM THANH</div>
          </div>
        </div>

        <div className='row pt-30'>
          <div className='col-lg-3 folder mr-20 ml-25' onDoubleClick={()=> showProfile('topos')}>
            <div className='item bg -blue-light mt-15'>
              <i class="fas fa-folder-open pt-13 pl-13"></i>
            </div>
            <div className='mt-25 pl-20'>DỮ LIỆU 3D ĐỊA HÌNH</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default List;
