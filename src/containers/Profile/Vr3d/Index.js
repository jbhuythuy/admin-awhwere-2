import { BASE_URL } from '../../../global/constant';
import '../list_profile.scss';
import 'filepond/dist/filepond.min.css';
import React, { useState, useEffect, useRef } from 'react';
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileEncode from 'filepond-plugin-file-encode';
import {VrService} from '../../../services/Profile/vr.service';
import toast from 'react-hot-toast';
import { ProfileService } from '../../../services/profile.service'
import moment from 'moment';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import fileSaver  from 'file-saver';
var JSZip = require("jszip");

registerPlugin(FilePondPluginFileValidateType, FilePondPluginFileEncode);

function Index(){
  const [files, setFiles] = useState([]);
  const [listfiles, setListFiles] = useState([]);
  const [heritage, setHeritage] = useState({});
  const [listHeritage, setListHeritage] = useState([]);
  const [show3DModal, setshow3DModal] = useState(false);

  const inputEl = useRef(null);
  const [roleAdmin, setRole] = useState(true)

  function createFile(files, heritage){

    let params_file =  files.map(file => ({
      type: file.fileType,
      size: file.fileSize,
      name: file.filename,
      base64: file.getFileEncodeBase64String()
    }))

    let data = {
      heritage_id: heritage.id,
      values: {
        vrs: params_file
      }
    }

    return VrService.create(data)
      .then((item) => {
        toast.success('Tạo file thành công');
        setFiles([]);
        VrService.getAll(heritage.id).then(item => setListFiles(item.data))
      })
  }

  function showFile(heritage_id, heritage_name){
    VrService.getAll(heritage_id).then(item => {
      setListFiles(item.data)
    })
    setHeritage({id: heritage_id, name: heritage_name})
  }

  function deleteFilevr(id){
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui-confirm'>
            <h1 className='title'>Bạn chắc chứ?</h1>
            <p className='content'>Bạn thực sự muốn xoá dữ liệu?</p>
            <button onClick={onClose}>Không</button>
            <button
              onClick={() => handleClickDelete(onClose, id)}
            >
              Có
            </button>
          </div>
        );
      }
    })
  }

  function downloadFilevr(url, filename) {
    var a = document.createElement('a');
    let newurl = url.replaceAll("'", "\"");
    const urlObject = JSON.parse(newurl);
    let urls = urlObject.files.map(item => {
      return (BASE_URL+"/file/"+urlObject.folder +  "/" + item)
    });
    // console.log(urls);
    var zip = new JSZip();
    function request(url) {
      return new Promise(function(resolve) {
        var httpRequest = new XMLHttpRequest();
        httpRequest.open("GET", url);
        httpRequest.onload = function() {
          var filename = url.replace(/.*\//g, "");
          zip.file(filename, this.responseText, { binary: true, createFolders: true });
          resolve()
        }
        httpRequest.send()
      })
    }

    Promise.all(urls.map(function(url) {
      return request(url)
    }))
        .then(function() {
          zip.generateAsync({
            type: "blob"
          })
              .then(function(content) {
                fileSaver.saveAs(content, filename);
              });
        })
  }

  function viewFilevr(url){
    setshow3DModal(true);
    let newurl = url.replaceAll("'", "\"");
    const urlObject = JSON.parse(newurl);
    let jpgFile,mtlFile,objFile;
    let folderUrl = BASE_URL+"/file/"+urlObject.folder +  "/";
    urlObject.files.forEach(item=>{
      if(item.includes(".jpg")) jpgFile = item;
      if(item.includes(".mtl")) mtlFile = item;
      if(item.includes(".obj")) objFile = item;
    });
  }

  function handleClickDelete(onClose, id){
    VrService.delete('heritage_vrs', id)
      .then(() => {
        setListFiles(items => items.filter(item => item.id !== id));
        toast.success('Xoá file thành công');
      });

    onClose();
  }

  function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(0) + ' ' + sizes[i];
  }

  useEffect(() => {
    ProfileService.getAll('vrs').then(item => setListHeritage(item.data))

    let info = JSON.parse(localStorage.getItem('currentUser'))
    let isAdmin = info.role === 'manager'
    setRole(isAdmin)
  }, []);

  return(
    <div className='row pl-0 pr-0 bg -blue-light detail-profile'>
      <div className='col-lg-12'>
        <div className='row pt-20 mb-10 justify-content-center'>
          <div className='col-lg-6 pl-0'>
            <a href={'/list-profile'} className='i-btn -primary ml-20'>
              Quay Lại
            </a>
            {roleAdmin &&
              <>
                { Object.keys(heritage).length > 0 &&
                  <button type='submit' className='i-btn -success ml-20'
                    onClick={() => createFile(files, heritage) }>Lưu</button>
                }
              </>
            }
          </div>
          <div className='col-lg-6'>
            <div className="i-breadcrumb float-right">
            <a className="i-breadcrumb -section">Admin</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <a className="i-breadcrumb -section">hồ sơ</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <div className="i-breadcrumb">Danh Sách</div>
          </div>
          </div>
        </div>
        <div className='row justify-content-center'>
          {roleAdmin &&
            <>
              { Object.keys(heritage).length > 0 &&
                <div className='col-lg-12 pl-0 pr-0 bg -white'>
                  <FilePond
                    files={files}
                    allowMultiple={true}
                    onupdatefiles={setFiles}
                    labelIdle='Tải Lên'
                    allowFileTypeValidation={true}
                    acceptedFileTypes= {['application/zip']}
                    labelFileTypeNotAllowed='File không hợp lệ'
                    fileValidateTypeLabelExpectedTypes='Yêu cầu chọn file zip'
                    allowFileEncode={true}
                  />
                </div>
              }
            </>
          }
          <div className='col-lg-12 bg -white min-height'>
            <div className='row'>
              <div className='col-lg-5 pb-20'>
                <div className='pt-20 pb-10'>
                  <h7 className='i-bold'>TÊN DI SẢN</h7>
                </div>
                <div className='col-lg-12 pl-0 pb-2'>
                  {/* <input type="text" class="i-input" placeholder='Tìm Kiếm' /> */}
                </div>
                <div className='scrollbar'>
                  {listHeritage && listHeritage.map(item => (
                    <div className={ `file-vr` } key={ item.id }
                      onClick={ ()=> showFile(item.id, item.name) } ref={inputEl}>
                      <div>
                        <i className="icon icon-3d mt-2"/>
                      </div>
                      <div className='pl-3'>
                        <p className='mb-0 pt-2 file-vr__content'>{ item.name }</p>
                        <p>{ item.place_name }</p>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              <div className='col-lg-7 pb-20'>
                <div className='pt-20 pb-10'>
                  <h7 className='i-bold uppercase'>DỮ LIỆU: { heritage.name }</h7>
                </div>
                <div className="i-table-wrap scrollbar">
                  <table className="i-table">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Tên File</th>
                        <th>Dung Lượng</th>
                        <th>Ngày Tạo</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      { listfiles && listfiles.map((file, index) => (
                        <tr key={ file.id }>
                          <td>{ index + 1 }</td>
                          <td>{ file.name }</td>
                          <td>{ bytesToSize(file.size)}</td>
                          <td>{ moment(file.create_date).format('L') }</td>
                          <td className="text-nowrap">
                            {roleAdmin &&
                              <>
                                {/* <a onClick={() => viewFilevr(file.url)} >
                                  <i className="icon icon-eye"></i>&nbsp;
                                </a> */}
                                <a className='cursor-pointer' onClick={() => downloadFilevr(file.url, file.name)}>
                                  <i className="fas fa-download"></i>&nbsp;
                                </a>
                                <a className='cursor-pointer' onClick={() => deleteFilevr(file.id)} >
                                  <i className="icon icon-trash"></i>
                                </a>
                              </>
                            }
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Index;
