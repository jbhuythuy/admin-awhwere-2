import React, { useState, useEffect } from 'react';
import './users.scss';
import avatar from '../../assets/resources/image/avatar.png';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import toast from 'react-hot-toast';
import ValidateForm from './Validate';
import { ApiService } from '../../services/api.service';
import { UserService } from '../../services/user.service';
import moment from 'moment';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

function Index(){
  const [isActive, setActive] = useState(false);
  const [isActiveInfo, setActiveInfo] = useState(false);
  const [listUsers, setListUsers] = useState([]);
  const [infoUser, setInfoUser] = useState({});
  const [listAreas, setListAreas] = useState([]);
  const validateForm = yup.object().shape(ValidateForm(yup));
  const [isCreate, setCreate] = useState(false);
  const [isId, setisId] = useState(null);
  const model_areas = 'places';
  const [limitPage, setLimitPage] = useState(5);
  const [totalPage, settotalPage] = useState(0)
  const [offsetPage, setoffsetPage] = useState(0)
  const [isSearch, setSearch] = useState(false);
  const [valueSearch, setvalueSearch] = useState('');
  const [currentPage, setcurrentPage] = useState(1);
  const [roleAdmin, setRole] = useState(true)

  const { register, handleSubmit, setValue,  reset, formState:{ errors }, clearErrors } = useForm({
    resolver: yupResolver(validateForm)
  });

  function showModalUser(check, id){
    const fields = ['name', 'login', 'email', 'role', 'password', 'place_id'];
    fields.forEach(field => clearErrors(field));

    if(check) {
      setisId(null);
      fields.forEach(field => setValue(field, ''));
    }
    else {
      setisId(id);

      UserService.getById(id)
        .then(obj => {
          const field_name = obj.data[0]
          fields.forEach(field => setValue(field, field_name[field]));
        });
    }
    setCreate(check);
    setActive(!isActive);
  }

  function showInfoUser(id){
    setActiveInfo(!isActiveInfo);
    UserService.getById(id).then(item => {
      if(item.data !== undefined) {
        setInfoUser(item.data[0])
      }
    });
  }

  useEffect(() => {
    UserService.getAll(limitPage, offsetPage).then(item => {
      setListUsers(item.data)
      settotalPage(Math.ceil(parseInt(item.total) / limitPage))
    });

    ApiService.getForAll(model_areas).then(item => setListAreas(item.data))

    let info = JSON.parse(localStorage.getItem('currentUser'))
    let isAdmin = info.role === 'manager'
    setRole(isAdmin)
  }, []);

  function onSubmit(data) {
    return isCreate
    ? createUser(data)
    : updateUser(data);
  }

  function createUser(data) {
    setActive(!isActive);

    UserService.create(data)
      .then(() => {
        toast.success('Tạo tài khoản thành công');

        UserService.getAll(limitPage, 0).then(item => {
          setListUsers(item.data)
          settotalPage(Math.ceil(parseInt(item.total) / limitPage))
        })
      })
  }

  function updateUser(data){
    setActive(!isActive);

    UserService.update(isId, data)
      .then(() => {
        toast.success('Cập nhật tài khoản thành công')
        UserService.getAll(limitPage, offsetPage).then(item => setListUsers(item.data))
      })
  }

  function deleteUser(id){
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui-confirm'>
            <h1 className='title'>Bạn chắc chứ?</h1>
            <p className='content'>Bạn thực sự muốn xoá dữ liệu?</p>
            <button onClick={onClose}>Không</button>
            <button
              onClick={() => handleClickDelete(onClose, id)}
            >
              Có
            </button>
          </div>
        );
      }
    })
  }

  function handleClickDelete(onClose, id){
    UserService.delete(id)
      .then(() => {
        setListUsers(items => items.filter(item => item.id !== id));
        toast.success('Xoá tài khoản thành công');
      });

    onClose();
  }

  function search(event) {
    if (event.charCode === 13) {
      if(event.target.value === ''){
        setSearch(!isSearch)

        UserService.getAll(limitPage, 0).then(item => {
          setListUsers(item.data)
          settotalPage(Math.ceil(parseInt(item.total) / limitPage))
        })
      }
      else {
        setSearch(!isSearch)
        setvalueSearch(event.target.value)

        UserService.search(`table.c.name.like(('%${ event.target.value }%'))`, limitPage, 0)
          .then(item => {
            setListUsers(item.data)
            settotalPage(Math.ceil(parseInt(item.total) / limitPage))
          })
      }
    }
  }

  function showPageChange(value) {
    const offset = limitPage * (value -1)
    setcurrentPage(value)
    setoffsetPage(offset)

    if(isSearch) {
      UserService.search(`table.c.name.like(('%${ valueSearch }%'))`, limitPage, offset)
        .then(item => {
          setListUsers(item.data)
        })
    }
    else {
      UserService.getAll(limitPage, offset).then(item => {
        setListUsers(item.data)
      })
    }
  }

  function showSetLimitPage(value){
    setLimitPage(value)

    UserService.getAll(value, 0).then(item => {
      setListUsers(item.data)
      settotalPage(Math.ceil(parseInt(item.total) / value))
    })
  }

  return(
    <div className='row pl-0 pr-0 bg -blue-light area'>
      <div className='col-lg-12'>
        <div className='row pt-15 mb-10'>
          <div className='col-lg-6'>
            <p className='uppercase font-13 mb-0'>Quản Lý Tài Khoản</p>
          </div>
          <div className='col-lg-6'>
            <div className="i-breadcrumb float-right">
            <a className="i-breadcrumb -section">Admin</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <a className="i-breadcrumb -section">tài khoản</a>
            <i className="i-breadcrumb -icon-divider"></i>
            <div className="i-breadcrumb">Danh Sách</div>
          </div>
          </div>
        </div>
        <div className='row pb-20 pt-20 bg -white'>
          <div className='col-lg-12'>
            <button class={`i-btn -brown`} disabled={roleAdmin ? '' : 'disabled'} onClick={() => showModalUser(true, null) }>
              <i class="fas fa-plus-circle mr-1 ml-n2"></i>
              tạo tài khoản
            </button>
          </div>
        </div>

        <div className='row pb-10 bg -white'>
          <div className='col-lg-9 search'>
            <p className='mr-3'>Hiển Thị</p>
            <div class="i-group">
              <select class="i-search" onChange={e => showSetLimitPage(parseInt(e.currentTarget.value))}>
                <option value="5">5</option>
                {/* <option value="20">20</option>
                <option value="50">50</option> */}
              </select>
            </div>
            <p className='ml-3'></p>
          </div>
          <div className='col-lg-3'>
            <input type="text" class="i-input" placeholder='Tìm Kiếm' onKeyPress={e => search(e)}/>
          </div>
        </div>
        <div className='row bg -white min-table'>
          <div className='col-lg-12'>
            <div class="i-table-wrap" >
              <table class="i-table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Tên Tài Khoản</th>
                    <th>Đăng nhập</th>
                    <th>Email</th>
                    <th>Vai Trò</th>
                    <th>Ngày Tạo</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {listUsers.length > 0 && listUsers.map((user, index) => (
                    <tr key={ user.id }>
                      <td>{ index + 1 }</td>
                      <td>{ user.name }</td>
                      <td>{ user.login }</td>
                      <td>{ user.email }</td>
                      <td>{ user.role }</td>
                      <td>{ moment(user.create_date).format('L') }</td>
                      <td className="text-nowrap">
                        <a className={`mr-2`} onClick={()=> showInfoUser(user.id)} >
                          <i className="icon icon-eye"></i>
                        </a>
                        {roleAdmin &&
                          <>
                            <a className={`mr-2`} onClick={()=> showModalUser(false, user.id)}>
                              <i className="icon icon-edit -sm"></i>
                            </a>
                            <a className='cursor-pointer' onClick={() => deleteUser(user.id)}>
                              <i className="icon icon-trash"></i>
                            </a>
                          </>
                        }

                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className='i-border-down'></div>
        <div className='row  bg -white pt-10'>
          <div className='col-lg-7'>
            {/* <p>1 den 5 cua 12</p> */}
          </div>
          <div className='col-lg-5 text-right'>
            <ul className="i-pagination">
              { currentPage > 1 &&
                <li className="i-pagination__item -text">
                  <a onClick={()=> showPageChange(currentPage - 1) }>
                    « Prev
                  </a>
                </li>
              }
              { currentPage > 1 &&
                <li className="i-pagination__item">
                  <a onClick={()=> showPageChange(currentPage - 1)}>
                    { currentPage - 1 }
                  </a>
                </li>
              }
              { totalPage > 0 &&
                <li className="i-pagination__item active">{currentPage}</li>
              }
              { totalPage > currentPage &&
                <li className="i-pagination__item">
                  <a onClick={()=> showPageChange(currentPage + 1)}>
                    {currentPage + 1}
                  </a>
                </li>}
              { totalPage > currentPage && <li className="i-pagination__item"><a>…</a></li> }
              { totalPage > currentPage &&
                <li className="i-pagination__item -text">
                  <a onClick={()=>showPageChange(currentPage + 1)}>
                    Next »
                  </a>
                </li>
              }
            </ul>
          </div>

        </div>
      </div>

      <div id='create-user-popup show' className={`i-popup modal fade ${ isActive ? 'show-modal': '' }`} role="dialog">
        <div className="modal-dialog">
          <div className="modal-content pt-30 pb-30 pl-30 pr-30">
            <button className="i-popup__close" onClick={()=> showModalUser(true, null)}>
              <i className="fas fa-times-circle"></i>
            </button>
            <h2 className="i-popup__title font-weight-bold">Tạo Tài Khoản</h2>
            <form onSubmit={ handleSubmit(onSubmit) } onReset={ reset }>
              <div className="mt-20 mb-10">
                <div className="i-group">
                  <div className="i-label i-text">Tên Hiện Thị:</div>
                  <input {...register('name')}
                    className={ `i-input ${ errors.name ? '-danger' : '' }`}
                    placeholder='Nhập Tên Hiện Thị'
                  />
                  { errors.name && (
                    <p className='error-danger mb-0'>{ errors.name?.message }</p>
                  )}
                </div>
                <div className="i-group">
                  <div className="i-label i-text">Tên Đăng Nhập：</div>
                  <input {...register('login')}
                    className={ `i-input ${ errors.login ? '-danger' : '' }`}
                    placeholder='Nhập Tên Đăng Nhập'
                  />
                  { errors.login && (
                    <p className='error-danger mb-0'>{ errors.login?.message }</p>
                  )}
                </div>
                <div className="i-group">
                  <div className="i-label i-text">Email</div>
                  <input {...register('email')}
                    className={ `i-input ${ errors.email ? '-danger' : '' }`}
                    placeholder='Nhập Email'
                  />
                  { errors.email && (
                    <p className='error-danger mb-0'>{ errors.email?.message }</p>
                  )}
                </div>
                <div className="i-dropdown">
                  <div className="i-dropdown__label">Vai Trò</div>
                  <div className="i-dropdown__pane">
                    <label className="">
                      <input {...register('role')}
                        type='radio'
                        value='manager'
                      />
                      <span className="ml-1">Manager</span>
                    </label>
                    <label className="ml-3">
                      <input {...register('role')}
                        type='radio'
                        value='guest'
                      />
                      <span className="ml-1">Guest</span>
                    </label>
                  </div>
                </div>
                { errors.role && (
                  <p className='error-danger mb-0'>{ errors.role?.message }</p>
                )}
                <div className="i-group">
                  <div className="i-label i-text">Mật Khẩu：</div>
                  <input {...register('password')}
                    className={ `i-input ${ errors.password ? '-danger' : '' }`}
                    type="password"
                    placeholder='Nhập Mật Khẩu'
                  />
                  { errors.password && (
                    <p className='error-danger mb-0'>{ errors.password?.message }</p>
                  )}
                </div>
                <div className="i-group">
                  <div className="i-label i-text">Địa Điểm ：</div>
                  <select {...register('place_id')} className={ `i-select ${ errors.place_id ? '-danger' : '' }`}>
                    <option value=''>Chọn giá trị</option>
                    {listAreas.length > 0 && listAreas.map(area => (
                      <option value={ area.id }>{ area.name }</option>
                    ))}
                  </select>
                  { errors.place_id && (
                    <p className='error-danger mb-0'>{ errors.place_id?.message }</p>
                  )}
                </div>
              </div>
              <div className="text-center mb-6">
                <button type="button" className="i-popup__btn i-btn -border -danger mr-3"
                  onClick={() => showModalUser(true, null) }>
                    Huỷ
                </button>
                <button type="submit" className="i-popup__btn i-btn -success -border">Lưu</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div id='show-user-popup show' className={ `i-popup modal fade ${ isActiveInfo ? 'show-modal': '' }`}>
        <div className="modal-dialog">
          <div className="modal-content pt-30 pb-30 pl-30 pr-30">
            <button className="i-popup__close" onClick={() => showInfoUser() }>
              <i className="fas fa-times-circle"></i>
            </button>
            <h2 className="i-popup__title font-weight-bold text-center">Thông Tin Tài Khoản</h2>
            <div className="mt-20 mb-10 row justify-content-center">
              <div className='col-lg-12 text-center'>
                <img src={avatar} alt=''/>
                <p className='font-weight-bold'>{ infoUser.name }</p>
              </div>
              <div className='col-lg-8 i-border-down'></div>
              <div className='col-lg-8 mt-3'>
                <div className='row'>
                  <div className='col-lg-4 pl-0'>
                    <p>Email:</p>
                    <p>Vai Trò:</p>
                    <p>Ngày Tạo:</p>
                    <p>Địa Điểm:</p>
                  </div>
                  <div className='col-lg-8'>
                    <p>{ infoUser.email }</p>
                    <p>{ infoUser.role }</p>
                    <p>{ moment(infoUser.create_date).format('L') }</p>
                    <p>{ infoUser.place_name }</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Index;