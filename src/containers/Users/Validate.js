
function ValidateForm(yup){
  let objectValidate = {
    name: yup.string().required('Tên không được trống'),
    login: yup.string().required('Tên đăng nhập không được trống'),
    email: yup.string().required('Email không được trống'),
    role: yup.string().nullable().default(null).required('Vai trò không được trống'),
    password: yup.string().required('Mật khẩu không được trống'),
    place_id: yup.string().required('Địa điểm không được trống')
  }
  return objectValidate;
};

export default ValidateForm;
