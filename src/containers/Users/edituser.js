import '../Users/users.scss';
import img from '../../assets/img/anime3.png';
import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';
import { UserService } from '../../services/user.service';
import { ApiService } from '../../services/api.service'
import {LogoutService} from '../../services/logout.service';

function Edituser () {
	const { register, handleSubmit, setError, setValue, reset, formState:{ errors }, clearErrors } = useForm({});
	const [infoUser, setInfoUser] = useState({});
	const [listAreas, setListAreas] = useState([]);
	const [idUser, setIdUser] = useState(null);
	const [button, setButton] = useState(1);
	const [message, setMessage] = useState(false);
	const [passCurrent, setPassCurrent] = useState(false);

	useEffect(() => {
		let info = JSON.parse(localStorage.getItem('currentUser'))
		setIdUser(info.id)
    ApiService.getForAll('places').then(item => {setListAreas(item.data)});
		UserService.getById(info.id).then(item => {
			if(item.data.length > 0){
				setInfoUser(item.data[0])
        const fields = ['name', 'login', 'email', 'place_id'];
				fields.forEach(field => clearErrors(field));
				fields.forEach(field => setValue(field, item.data[0][field]));
			}
		})
  }, []);

	function onSubmit(data){
		if(button === 1){
      delete data.password
			delete data.password_old
			delete data.password_confirm
      UserService.update(idUser, data)
				.then(() => {
					toast.success('Cập nhật tài khoản thành công')
					UserService.getById(idUser).then(item => {
						if(item.data.length > 0){
							setInfoUser(item.data[0])
							const fields = ['name', 'login', 'email', 'place_id'];
							fields.forEach(field => clearErrors(field));
							fields.forEach(field => setValue(field, item.data[0][field]));
						}
					})
				})
		}
		else if(button === 2){
			if(data.password !== '' && data.password_confirm !== '' && data.password_old !== ''){

				if(data.password === data.password_confirm){
					setMessage(false)
					setPassCurrent(false)

					UserService.update(idUser, { password: data.password })
						.then(() => {
							const token = localStorage.getItem('token')
							LogoutService.logout({token: token})
								.then(() => {
									toast.success('Cập nhật tài khoản thành công')
									localStorage.removeItem('currentUser');
									localStorage.removeItem('token');
									window.location.reload();
								})
						})
				}
				else{
					setMessage(true)
					setPassCurrent(true)
				}
			}
			else {
				if(data.password_old === ''){
				  setPassCurrent(true)
				}
				else {
          setPassCurrent(false)
				}

				if(data.password === ''){
					setMessage(true)
				}

				else if(data.password_confirm === ''){
					setMessage(true)
				}
				else {
					setMessage(false)
				}
			}
		}
	}

	return (
		<div className='col-md-12 edit-user'>
			<div className='row'>
				<div className='col-md-8 card-edit'>
					<h5 className='title pt-2 pb-4'>Thông Tin Người Dùng</h5>
					<form onSubmit={ handleSubmit(onSubmit) } onReset={ reset }>
						<div className='row'>
							<div className='input-container col-md-6'>
								<label className='label ml-2'>Họ & Tên</label>
								<input {...register('name')}
									className={ `name-box`}
									placeholder='Tên Hiện Thị'
								/>
							</div>
							<div className='input-container col-md-6'>
								<label className='label ml-2'>Tên Đăng Nhập</label>
								<input {...register('login')}
									className={ `username-box`}
									disabled
									placeholder="Tên đăng nhập"
								/>
							</div>
							<div className='input-container col-md-6 mt-4'>
								<label className='label ml-2'>Email</label>
								<input {...register('email')}
									className={ `address-box`}
									placeholder='Địa chỉ mail'
								/>
							</div>
							<div className='input-container col-md-6 mt-4'>
								<label className='label ml-2'>Đơn Vị Trực Thuộc</label>
								<select {...register('place_id')} className={ `address-box`}>
									<option value=''></option>
									{listAreas && listAreas.map(area => (
										<option key={ area.id } value={ area.id }>{ area.name }</option>
									))}
								</select>
							</div>

							<div className='col-md-12'>
								<button type='submit' className='i-btn -success col-md-3 float-right' onClick={()=> setButton(1)}>
									Cập Nhật
								</button>
							</div>
							<div className='col-md-12 mt-4'>
								<h5 className='title'>Thay Đổi Mật Khẩu</h5>
							</div>
							<div className='input-container col-md-4'>
								<label className='label ml-2'>Mật khẩu hiện tại</label>
								<input {...register('password_old')}
									className={ `address-box`}
									type="password"
									placeholder='Nhập mật khẩu hiện tại'
								/>
								<p className={`error-danger mb-0 ${passCurrent ? 'show_message' : 'hide_message'}`}>Nhập mật khẩu hiện tại.</p>
							</div>
							<div className='input-container col-md-4'>
								<label className='label ml-2'>Mật khẩu mới</label>
								<input {...register('password')}
									className={ `address-box`}
									type="password"
									placeholder='Nhập mật khẩu mới'
								/>
								<p className={`error-danger mb-0 ${message ? 'show_message' : 'hide_message'}`}>Mật khẩu xác nhận không đúng.</p>
							</div>
							<div className='input-container col-md-4'>
								<label className='label ml-2'>Xác nhận mật khẩu</label>
								<input {...register('password_confirm')}
									className={ `address-box`}
									type="password"
									placeholder='Nhập lại mật khẩu mới'
								/>
							</div>
							<div className='col-md-12'>
								<button type='submit' className='i-btn -success col-md-3 float-right' onClick={()=> setButton(2)}>
									Đổi Mật Khẩu
								</button>
							</div>
							<div className='col-md-12 mt-4 pb-4'>
								<h5 className='title'>Lưu Ý:</h5>
								<p>- Người dùng cần thực hiện thay đổi mật khẩu thường xuyên, theo chu kỳ ít nhất là 6 tháng, để đảm bảo an toàn thông tin và toàn vẹn dữ liệu.</p>
								<p>- Đặt mật khẩu trên 8 ký tự, bao gồm chữ cái in hoa, số và ký tự đặc biệt</p>
								<p>- Trong trường hợp để lộ mật khẩu cần thực hiện đăng nhập và đổi lại, nếu không được liên hệ ngay với quản trị hệ thống để được xử lý kịp thời</p>
								<p>- Các hành vi, thao tác thực hiện trên hệ thống cần được thực hiện đúng theo nghiệp vụ và trách nhiệm được giao, các hành động gây mất mát dữ liệu sẽ chịu trách nhiệm trước pháp luật</p>
							</div>
						</div>
					</form>
				</div>
				<div className='col-md-4'>
					<div className='row'>
						<div className='user-view ml-4'>
							<div className="author">
								<div className="block block-one" />
								<div className="block block-two" />
								<div className="block block-three" />
								<div className="block block-four" />
								<a href="#pablo" onClick={(e) => e.preventDefault()}>
									<img
										alt="..."
										className="avatar"
										src={img}
									/>
									<h5 className="title pt-4">{infoUser.name !== undefined && infoUser.name}</h5>
								</a>
								<p className="description">{infoUser.role !== undefined && infoUser.role}</p>
								<p className='pt-4 pb-4 pl-4 pr-4'></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Edituser;
