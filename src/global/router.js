import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import {
  HOME_PAGE,
  HOME,
  TYPES,
  LIST_PROFILE,
  DETAIL_PROFILE,
  LIST_USERS,
  DATA_RATING,
  DATA_AREAS,
  DATA_TYPE,
  DATA_NATION,
  LIST_VR,
  LIST_VR3D,
  LIST_IMAGES,
  LIST_VIDEOS,
  LIST_DOCUMENTS,
  LIST_SOUNDS,
  LIST_DIAHINH,
  HERITAGE,
  HERITAGE_NEW,
  HERITAGE_SHOW,
  HERITAGE_EDIT,
  USER_INFOMATION,
  NEWS_MANAGER,
  ADD_NEWS,
  NEWS_EDIT,
  NEWS_SHOW
} from './constant';

const Loading = () => null;

const routes = [
  {
    path: HOME,
    component: Loadable({
      loader: () =>
        import('../containers/Home/Home'),
      loading: Loading,
      modules: ['Home'],
    }),
    exact: true
  },
  {
    path: HOME_PAGE,
    component: Loadable({
      loader: () =>
        import('../containers/Home/Home'),
      loading: Loading,
      modules: ['Home'],
    }),
    exact: true
  },
  {
    path: HERITAGE,
    component: Loadable({
      loader: () =>
        import('../containers/Heritage/Index'),
      loading: Loading,
      modules: ['Heritage'],
    })
  },
  // {
  //   path: AREA,
  //   component: Loadable({
  //     loader: () =>
  //       import('../containers/Heritage/Area/Index'),
  //     loading: Loading,
  //     modules: ['Area'],
  //   })
  // },
  {
    path: HERITAGE_NEW,
    component: Loadable({
      loader: () =>
        import('../containers/Heritage/New'),
      loading: Loading,
      modules: ['Heritage'],
    })
  },
  {
    path: HERITAGE_SHOW,
    component: Loadable({
      loader: () =>
        import('../containers/Heritage/Show'),
      loading: Loading,
      modules: ['Heritage'],
    })
  },
  {
    path: HERITAGE_EDIT,
    component: Loadable({
      loader: () =>
        import('../containers/Heritage/Edit'),
      loading: Loading,
      modules: ['Heritage'],
    })
  },
  {
    path: TYPES,
    component: Loadable({
      loader: () =>
        import('../containers/Heritage/Index'),
      loading: Loading,
      modules: ['Types'],
    })
  },
  {
    path: LIST_PROFILE,
    component: Loadable({
      loader: () =>
        import('../containers/Profile/List'),
      loading: Loading,
      modules: ['Profile'],
    })
  },
  {
    path: DETAIL_PROFILE,
    component: Loadable({
      loader: () =>
        import('../containers/Profile/Detail'),
      loading: Loading,
      modules: ['Profile'],
    })
  },
  {
    path: DATA_RATING,
    component: Loadable({
      loader: () =>
        import('../containers/MasterData/Rating/Index'),
      loading: Loading,
      modules: ['MasterData'],
    })
  },
  {
    path: DATA_AREAS,
    component: Loadable({
      loader: () =>
        import('../containers/MasterData/Areas/Index'),
      loading: Loading,
      modules: ['MasterData'],
    })
  },
  {
    path: DATA_TYPE,
    component: Loadable({
      loader: () =>
        import('../containers/MasterData/Type/Index'),
      loading: Loading,
      modules: ['MasterData'],
    })
  },
  {
    path: DATA_NATION,
    component: Loadable({
      loader: () =>
        import('../containers/MasterData/Nation/Index'),
      loading: Loading,
      modules: ['MasterData'],
    })
  },
  {
    path: LIST_USERS,
    component: Loadable({
      loader: () =>
        import('../containers/Users/Index'),
      loading: Loading,
      modules: ['Users'],
    })
  },
  {
    path: LIST_VR,
    component: Loadable({
      loader: () =>
        import('../containers/Profile/Vr360/Index'),
      loading: Loading,
      modules: ['Vr360'],
    })
  },
  {
    path: LIST_VR3D,
    component: Loadable({
      loader: () =>
        import('../containers/Profile/Vr3d/Index'),
      loading: Loading,
      modules: ['Vr3d'],
    })
  },
  {
    path: LIST_IMAGES,
    component: Loadable({
      loader: () =>
        import('../containers/Profile/Images/Index'),
      loading: Loading,
      modules: ['Images'],
    })
  },
  {
    path: LIST_VIDEOS,
    component: Loadable({
      loader: () =>
        import('../containers/Profile/Videos/Index'),
      loading: Loading,
      modules: ['Videos'],
    })
  },
  {
    path: LIST_DOCUMENTS,
    component: Loadable({
      loader: () =>
        import('../containers/Profile/Documents/Index'),
      loading: Loading,
      modules: ['Documents'],
    })
  },
  {
    path: LIST_SOUNDS,
    component: Loadable({
      loader: () =>
        import('../containers/Profile/Sounds/Index'),
      loading: Loading,
      modules: ['Sounds'],
    })
  },
  {
    path: LIST_DIAHINH,
    component: Loadable({
      loader: () =>
        import('../containers/Profile/Diahinh/Index'),
      loading: Loading,
      modules: ['Diahinh'],
    })
  },
  {
    path: USER_INFOMATION,
    component: Loadable({
      loader: () =>
        import('../containers/Users/edituser'),
      loading: Loading,
      modules: ['Sounds'],
    })
  },
  {
    path: NEWS_MANAGER,
    component: Loadable({
      loader: () =>
        import('../containers/News/News'),
      loading: Loading,
      modules: ['Sounds'],
    })
  },
  {
    path: ADD_NEWS,
    component: Loadable({
      loader: () =>
        import('../containers/News/add-news'),
      loading: Loading,
      modules: ['Sounds'],
    })
  },
  {
    path: NEWS_SHOW,
    component: Loadable({
      loader: () =>
          import('../containers/News/Show'),
      loading: Loading,
      modules: ['Sounds'],
    })
  },
  {
    path: NEWS_EDIT,
    component: Loadable({
      loader: () =>
          import('../containers/News/Edit'),
      loading: Loading,
      modules: ['Sounds'],
    })
  },
];

const Routes = () => {
  return (
    <Switch>
      { routes.map(({ path, component, exact = false }) => (
        <Route key={path} path={path} exact={exact} component={component} />
      ))}
    </Switch>
  );
};

export default Routes;
