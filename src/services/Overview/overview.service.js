import { fetchWrapper } from '../../utils/http/fetch-wrapper';
const baseUrl = 'http://10.128.195.192:5000/api/overview';
const appServer1URL = "http://10.128.195.192:5000/api/serverinfo";

export const OverviewService = {
  overview, appServer,
};

function overview(){
    return fetchWrapper.post(baseUrl, {});
}

function appServer(){
    return fetchWrapper.get(appServer1URL, {});
}
