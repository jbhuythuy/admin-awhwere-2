
import { fetchWrapper } from '../../utils/http/fetch-wrapper';
import { BASE_URL } from '../../global/constant';

export const DocumentService = {
  getAll,
  create,
  delete: _delete
};

function getAll(heritage_id) {
  return fetchWrapper.post(`${BASE_URL}/api/heritage/docs`, {heritage_id: heritage_id});
}

function create(values) {
  return fetchWrapper.post(`${BASE_URL}/api/heritage/update_file`, values);
}

function _delete(model, id) {
  let params_delete = {
    'model': model,
    'id': id
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritage/delete_detail`, params_delete);
}
