import { fetchWrapper } from '../utils/http/fetch-wrapper';
import { BASE_URL } from '../global/constant';

export const ApiService = {
  getAll,
  getById,
  create,
  update,
  search,
  getForAll,
  getAllNews,
  searchNews,
  delete: _delete
};

function getAll(model, limit, offset) {
  var params_list = {
    'model': model,
    'limit': limit,
    'offset': offset
  }

  return fetchWrapper.post(`${BASE_URL}/api/list`, params_list);
}

function getForAll(model) {
  var params_all = {
    'model': model
  }

  return fetchWrapper.post(`${BASE_URL}/api/list`, params_all);
}

function getById(model, ids) {
  let params_read = {
    'model': model,
    'ids': ids
  }

  return fetchWrapper.post(`${BASE_URL}/api/read`, params_read);
}

function create(model, values) {
  let params_create = {
    'model': model,
    'values': values
  }

  return fetchWrapper.post(`${BASE_URL}/api/create`, params_create);
}

function update(model, id, values) {
  let params_update = {
    'model': model,
    'id': id,
    'values': values
  }

  return fetchWrapper.post(`${BASE_URL}/api/update`, params_update);
}

function _delete(model, id) {
  let params_delete = {
    'model': model,
    'id': id
  }

  return fetchWrapper.post(`${BASE_URL}/api/delete`, params_delete);
}

function search(model, domain, limit, offset) {
  let params_search = {
    'model': model,
    'domain': domain,
    'limit': limit,
    'offset': offset
  }

  return fetchWrapper.post(`${BASE_URL}/api/search`, params_search);
}

function getAllNews(limit, offset, fields=[]){
  let params = {
    'limit': limit,
    'offset': offset,
    'fields': fields
  }

  return fetchWrapper.post(`${BASE_URL}/api/posts/list`, params);
}

function searchNews(domain, limit, offset){
  let params_new = {
    'domain': domain,
    'limit': limit,
    'offset': offset
  }

  return fetchWrapper.post(`${BASE_URL}/api/posts/search`, params_new);
}
