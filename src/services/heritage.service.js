import { fetchWrapper } from '../utils/http/fetch-wrapper';
import { BASE_URL } from '../global/constant';

export const HeritageService = {
  getAll,
  getById,
  create,
  update,
  search,
  filterPlace,
  getAllList,
  getAllPlaces,
  getAllByPlace,
  getAllHeritages,
  delete: _delete,
  searchName
};

function getAll(kind, limit, offset) {
  var params_list = {
    'kind': kind,
    'limit': limit,
    'offset': offset
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritages/list_from_kind`, params_list);
}

function getAllByPlace(kind, place_id, limit, offset) {
  var params_list = {
    'kind': kind,
    'place_id': place_id,
    'limit': limit,
    'offset': offset
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritages/list_from_kind`, params_list);
}

function getAllList(place_id, limit, offset) {
  var params_list = {
    'place_id': place_id,
    'limit': limit,
    'offset': offset,
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritages/list`, params_list);
}

function getAllHeritages() {
  return fetchWrapper.post(`${BASE_URL}/api/heritages/list_parent`, {});
}

function getAllPlaces() {
  var params_list = {
    model: 'places'
  }
  return fetchWrapper.post(`${BASE_URL}/api/list`, params_list);
}

function getById(ids) {
  let params_read = {
    'id': ids
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritages/read`, params_read);
}

function create(values) {
  let params_create = {
    'values': values
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritages/create`, params_create);
}

function update(id, values) {
  let params_update = {
    'id': id,
    'values': values
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritages/update`, params_update);
}

function _delete(id) {
  let params_delete = {
    'id': id
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritages/delete`, params_delete);
}

function search(domain, limit, offset) {
  let params_search = {
    'model': 'heritages',
    'domain': domain,
    'limit': limit,
    'offset': offset
  }

  return fetchWrapper.post(`${BASE_URL}/api/search`, params_search);
}

function searchName(s, limit, offset) {
  let params_search = {
    'model': 'heritages',
    's': s,
    'limit': limit,
    'offset': offset
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritages/search`, params_search);
}

function filterPlace(place, condition, limit, offset) {
  let params_search = {
    'place_id': place,
    'condition': condition,
    'limit': limit,
    'offset': offset
  }
  return fetchWrapper.post(`${BASE_URL}/api/heritages/filter-place`, params_search);
}
