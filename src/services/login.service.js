import { fetchWrapper } from '../utils/http/fetch-wrapper';
const baseUrl = 'http://10.128.195.192:5000/account/login';


export const LoginService = {
  login,
};

function login(params) {
  return fetchWrapper.post(baseUrl, params);
}
