import { fetchWrapper } from '../utils/http/fetch-wrapper';
const baseUrl = 'http://10.128.195.192:5000/account/logout';

export const LogoutService = {
  logout
};

function logout(params) {
  return fetchWrapper.post(baseUrl, params);
}
