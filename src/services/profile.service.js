import { fetchWrapper } from '../utils/http/fetch-wrapper';
import { BASE_URL } from '../global/constant';

export const ProfileService = {
  getAll
};

function getAll(type) {
  var params_list = {
    'type': type
  }

  return fetchWrapper.post(`${BASE_URL}/api/heritages/list_from_file_type`, params_list);
}
