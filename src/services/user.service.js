import { fetchWrapper } from '../utils/http/fetch-wrapper';
import { BASE_URL } from '../global/constant';

export const UserService = {
  getAll,
  getById,
  create,
  update,
  search,
  getForAll,
  delete: _delete
};

function getAll(limit, offset) {
  var params_list = {
    'limit': limit,
    'offset': offset
  }

  return fetchWrapper.post(`${BASE_URL}/api/users/list`, params_list);
}

function getForAll() {

  return fetchWrapper.post(`${BASE_URL}/api/users/list`, {});
}

function getById(ids) {
  let params_read = {
    'ids': ids
  }

  return fetchWrapper.post(`${BASE_URL}/api/users/read`, params_read);
}

function create(values) {
  let params_create = {
    'values': values
  }

  return fetchWrapper.post(`${BASE_URL}/api/users/create`, params_create);
}

function update(id, values) {
  let params_update = {
    'id': id,
    'values': values
  }

  return fetchWrapper.post(`${BASE_URL}/api/users/update`, params_update);
}

function _delete(id) {
  let params_delete = {
    'id': id
  }

  return fetchWrapper.post(`${BASE_URL}/api/users/delete`, params_delete);
}

function search(domain, limit, offset) {
  let params_search = {
    'domain': domain,
    'limit': limit,
    'offset': offset
  }

  return fetchWrapper.post(`${BASE_URL}/api/users/search`, params_search);
}
